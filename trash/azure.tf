locals {
  # az account list-locations | jq '.[].displayName'
  # azurerm_location = "North Europe"
}

resource "azurerm_resource_group" "default" {
  provider = azurerm.sp
  name     = var.rg_name
  location = var.location
  tags = {
    "urn:fdc:schema.org:maintainer" = "iwan.aucamp@coop.no"
  }
}


data "azuread_user" "default" {
  user_principal_name = "iwan.aucamp@coop.no"
}

data "azuread_users" "all" {
  user_principal_names = ["iwan.aucamp@coop.no", "filip1.stawicki@coop.no"]
}

locals {
  azuread_users = { for user in data.azuread_users.all.users : user.mail => user }
}

# data "null_data_source" "azuread_users" {
#   inputs = {
#     users = {for user in data.azuread_users.all.users: user.mail => user}
#   }
# }

resource "azurerm_role_assignment" "default" {
  provider = azurerm.sp
  for_each = toset([
    # data.azuread_user.default.id,
    local.azuread_users["iwan.aucamp@coop.no"].object_id,
  ])
  scope                = azurerm_resource_group.default.id
  role_definition_name = "Owner"
  principal_id         = each.value
}

resource "azurerm_key_vault" "default" {
  provider                    = azurerm
  name                        = var.kv_name
  tags                        = azurerm_resource_group.default.tags
  location                    = var.location
  resource_group_name         = var.rg_name
  enabled_for_disk_encryption = true
  tenant_id                   = var.tenant_id
  sku_name                    = "standard"

  network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }
}


resource "azurerm_key_vault_access_policy" "default" {
  for_each = toset([
    local.azuread_users["iwan.aucamp@coop.no"].object_id,
    # var.client_id,
  ])
  key_vault_id            = azurerm_key_vault.default.id
  tenant_id               = var.tenant_id
  object_id               = each.value
  secret_permissions      = ["Get", "List", "Set", "Delete", "Purge", "Recover"]
  key_permissions         = ["Get", "List", "Create", "Delete", "Update", "Purge", "Recover"]
  storage_permissions     = ["Get", "List", "Set", "Delete", "Update", "Purge"]
  certificate_permissions = ["Get", "List", "Create", "Delete", "Update", "Purge", "Recover"]
}


data "azurerm_resource_group" "default" {
  name       = var.rg_name
  depends_on = [resource.azurerm_resource_group.default]
}

data "azurerm_key_vault" "default" {
  name                = var.kv_name
  resource_group_name = var.rg_name
  depends_on          = [resource.azurerm_resource_group.default, resource.azurerm_key_vault.default]
}


# resource "azurerm_storage_account" "ooshaemu" {
#   #tfsec:ignore:AZU015
#   name                = "ooshaemu${var.environment}"
#   resource_group_name = azurerm_resource_group.default.name
#   location            = local.azurerm_location

#   account_kind             = "StorageV2"
#   account_tier             = "Standard"
#   account_replication_type = "RAGRS"
# }

# resource "azurerm_storage_container" "nahzucha" {
#   name                  = "nahzucha"
#   storage_account_name  = azurerm_storage_account.ooshaemu.name
#   container_access_type = "private"
# }

# resource "google_secret_manager_secret" "ooshaemu_ak" {
#   secret_id = "ooshaemu_ak"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "ooshaemu_ak" {
#   secret      = google_secret_manager_secret.ooshaemu_ak.id
#   secret_data = azurerm_storage_account.ooshaemu.primary_access_key
# }


# data "azurerm_storage_account_sas" "ooshaemu_sas" {
#   connection_string = azurerm_storage_account.ooshaemu.primary_connection_string

#   resource_types {
#     service   = false
#     container = true
#     object    = false
#   }

#   services {
#     blob  = true
#     queue = false
#     table = false
#     file  = false
#   }

#   start  = "2021-01-01T00:00:00Z"
#   expiry = format("%d-01-01T00:00:00Z", tonumber(formatdate("YYYY", timestamp())) + 2)

#   permissions {
#     read    = true
#     write   = false
#     delete  = false
#     list    = true
#     add     = false
#     create  = false
#     update  = false
#     process = false
#   }
# }

# output "sas_url_query_string" {
#   value = data.azurerm_storage_account_sas.ooshaemu_sas.sas
#   sensitive = true
# }

# resource "google_secret_manager_secret" "ooshaemu_sas" {
#   secret_id = "ooshaemu_sas"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "ooshaemu_sas" {
#   secret      = google_secret_manager_secret.ooshaemu_sas.id
#   secret_data = data.azurerm_storage_account_sas.ooshaemu_sas.sas
# }

# resource "azuread_application" "gcp_processor" {
#   display_name               = "gcp_processor"
#   available_to_other_tenants = false
# }

# resource "azuread_application" "gcp_processor" {
#   display_name = "gcp_processor"
#   feature_tags {
#     enterprise = true
#   }
# }
# resource "azuread_service_principal" "project_serviceprincipal" {
#   application_id               = azuread_application.gcp_processor.application_id
#   app_role_assignment_required = false
# }

