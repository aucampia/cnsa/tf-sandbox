
###############################################################################
# cloud build
###############################################################################

# https://cloud.google.com/build/docs/subscribe-build-notifications
resource "google_pubsub_topic" "cloud_builds" {
  name                       = "cloud-builds"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "cloud_builds" {
  name                       = "cloud-builds"
  topic                      = google_pubsub_topic.cloud_builds.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

# DONT COPY !!!!
resource "google_project_iam_member" "cloud_build_sa" {
  for_each = toset([
    "roles/cloudbuild.serviceAgent",
  ])
  project = data.google_project.project.project_id
  role    = each.value
  member  = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudbuild.iam.gserviceaccount.com"
}

resource "google_storage_bucket" "cloud_build_logs" {
  name     = "coopnorge-products-${var.environment}-cblogs"
  location = "europe-north1"

  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]

  lifecycle_rule {
    condition {
      age = 1
    }
    action {
      type = "Delete"
    }
  }
}
