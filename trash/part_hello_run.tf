###############################################################################
# project level
###############################################################################

resource "google_service_account" "hello_run" {
  account_id   = "hello-run"
  display_name = "hello-run"
}

resource "google_service_account" "hello_run_cd" {
  account_id   = "hello-run-cd"
  display_name = "hello-run-cd"
}


###############################################################################
# variables
###############################################################################

locals {
  hello_run_sa_id    = "hello-run"
  hello_run_sa_email = "${local.hello_run_sa_id}@${var.project_id}.iam.gserviceaccount.com"

  hello_run_cd_sa_id    = "hello-run-cd"
  hello_run_cd_sa_email = "${local.hello_run_cd_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}

###############################################################################
# service account
###############################################################################

data "google_service_account" "hello_run" {
  account_id = local.hello_run_sa_id

  depends_on = [
    google_service_account.hello_run
  ]
}


# data "google_service_account" "hello_run_cd" {
#   account_id = local.hello_run_cd_sa_id

#   depends_on = [
#     google_service_account.hello_run_cd
#   ]
# }

# ###############################################################################
# # storage
# ###############################################################################

# resource "google_storage_bucket" "hello_run" {
#   name     = "coopnorge-${var.project_key}-${var.environment}-hello-run"
#   location = var.region
# }

# resource "google_storage_bucket_iam_member" "hello_run" {
#   for_each = { for item in [
#     {
#       member = "serviceAccount:${local.hello_run_sa_email}",
#       role   = "roles/storage.objectViewer",
#     },
#     ] :
#     "${item.member}/${item.role}" => item
#   }
#   bucket = google_storage_bucket.hello_run.name
#   role   = each.value.role
#   member = each.value.member
# }

# ###############################################################################
# # OCI
# ###############################################################################

# resource "google_artifact_registry_repository" "hello_run" {
#   provider      = google-beta
#   location      = var.region
#   repository_id = "hello-run"
#   format        = "DOCKER"
# }

# resource "google_artifact_registry_repository_iam_member" "hello_run" {
#   provider = google-beta
#   for_each = merge([for item in [
#     {
#       member = "serviceAccount:${local.hello_run_sa_email}",
#       roles  = ["roles/artifactregistry.reader"],
#     },
#     {
#       member = "serviceAccount:${local.hello_run_cd_sa_email}",
#       roles  = ["roles/artifactregistry.writer"],
#     },
#     ] : {
#     for role in item.roles :
#     "${item.member}/${role}" => { member = item.member, role = role }
#     }
#   ]...)

#   repository = google_artifact_registry_repository.hello_run.name
#   location   = var.region

#   role   = each.value.role
#   member = each.value.member
# }

# ###############################################################################
# # cloud build
# ###############################################################################


# resource "google_pubsub_topic" "hello_run_cd" {
#   name                       = "hello-run-cd"
#   message_retention_duration = "${7 * 24 * 60 * 60}s"
# }

# resource "google_pubsub_subscription" "hello_run_cd" {
#   name                       = "hello-run-cd"
#   topic                      = google_pubsub_topic.hello_run_cd.id
#   ack_deadline_seconds       = 600
#   message_retention_duration = "${7 * 24 * 60 * 60}s"
#   labels = {
#     app = "hello-run"
#   }
# }

# resource "google_cloud_scheduler_job" "hello_run_cd" {
#   region      = "europe-central2"
#   name        = "hello-run-cd"
#   description = "hello-run-cd"

#   pubsub_target {
#     topic_name = google_pubsub_topic.hello_run_cd.id
#     attributes = {
#       action = "run"
#     }
#   }
# }

# # https://cloud.google.com/build/docs/build-config-file-schema
# resource "google_cloudbuild_trigger" "hello_run_cd" {
#   name = "hello-run-cd"

#   pubsub_config {
#     topic                 = google_pubsub_topic.hello_run_cd.id
#     service_account_email = data.google_service_account.hello_run_cd.email
#   }
#   service_account = data.google_service_account.hello_run_cd.id

#   # https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values
#   # https://cloud.google.com/build/docs/configuring-builds/use-bash-and-bindings-in-substitutions
#   substitutions = {
#     # This is the pubsub message body.
#     _PUBSUB_MESSAGE_BODY = "$(body)"
#   }

#   build {
#     step {
#       name       = "docker.io/rclone/rclone:1@sha256:1e6eeabddc013302793aad1986af0b8929bb42bb7c616b10a9b66b2ce04e165f"
#       entrypoint = "sh"
#       args = ["-xeuo", "pipefail", "-c",
#         <<-EOT
#         printenv PUBSUB_MESSAGE_BODY
#         export RCLONE_CONFIG_SRC_SERVICE_PRINCIPAL_FILE=$(mktemp)
#         printenv SERVICE_PRINCIPAL_JSON > $$${RCLONE_CONFIG_SRC_SERVICE_PRINCIPAL_FILE}
#         rclone move src:${local.pim_product_dumps_azure_storage_container}/ dst:${google_storage_bucket.pim_product_dumps.name}/
#         EOT
#       ]
#       env = [
#         "PUBSUB_MESSAGE_BODY=$_PUBSUB_MESSAGE_BODY",
#         "RCLONE_USE_JSON_LOG=true",
#         "RCLONE_LOG_FORMAT=date,time,microseconds,pid,longfile,shortfile,UTC",
#         "RCLONE_LOG_LEVEL=INFO",
#         "RCLONE_STATS=5s",
#         "RCLONE_CHECKSUM=true",
#         "RCLONE_STATS_ONE_LINE_DATE=true",
#         "RCLONE_CONFIG_SRC_TYPE=azureblob",
#         "RCLONE_CONFIG_SRC_ACCOUNT=${local.pim_azure_storage_account}",
#         "RCLONE_CONFIG_DST_TYPE=gcs",
#         "RCLONE_CONFIG_DST_GCS_PROJECT_NUMBER=${data.google_project.project.number}",
#         "RCLONE_CONFIG_DST_GCS_LOCATION=${local.gcs_location}",
#       ]
#       secret_env = ["SERVICE_PRINCIPAL_JSON"]
#     }
#     available_secrets {
#       secret_manager {
#         env          = "SERVICE_PRINCIPAL_JSON"
#         version_name = "${google_secret_manager_secret.pim_syncer_sp_json.id}/versions/latest"
#       }
#     }
#     logs_bucket = google_storage_bucket.cloud_build_logs.name
#   }
# }



###############################################################################
# cloud run
###############################################################################

# resource "google_secret_manager_secret" "hello_run_image" {
#   secret_id = "pim-sp-id"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "hello_run_image" {
#   secret      = google_secret_manager_secret.hello_run_image.id
#   secret_data = ""
# }



# resource "google_cloud_run_service" "hello_run" {
#   name     = "hello-run"
#   location = var.region

#   template {
#     spec {
#       containers {
#         image = "docker.io/rclone/rclone:1@sha256:1e6eeabddc013302793aad1986af0b8929bb42bb7c616b10a9b66b2ce04e165f"

#         env {
#           name  = "RCLONE_USE_JSON_LOG"
#           value = "true"
#         }
#         env {
#           name  = "RCLONE_LOG_FORMAT"
#           value = "date,time,microseconds,pid,longfile,shortfile,UTC"
#         }
#         env {
#           name  = "RCLONE_LOG_LEVEL"
#           value = "DEBUG"
#         }
#         env {
#           name  = "RCLONE_CONFIG_SRC_TYPE"
#           value = "gcs"
#         }
#         env {
#           name  = "RCLONE_CONFIG_SRC_GCS_PROJECT_NUMBER"
#           value = "750603684248"
#         }
#         env {
#           name  = "RCLONE_CONFIG_SRC_GCS_LOCATION"
#           value = "europe-north1"
#         }

#         command = ["sh", "-c",
#           <<-EOT
#           set -x -eo pipefail
#           env
#           export RCLONE_ADDR=:$${PORT}
#           rclone serve http --read-only src:${google_storage_bucket.hello_run.name}/
#           EOT
#         ]
#       }
#       container_concurrency = 100
#       service_account_name  = data.google_service_account.hello_run.email
#       timeout_seconds       = (9.5) * 60
#     }
#     metadata {
#       # https://cloud.google.com/run/docs/reference/rest/v1/RevisionTemplate
#       annotations = {
#         "autoscaling.knative.dev/minScale" = 0
#         "autoscaling.knative.dev/maxScale" = 1
#         "run.googleapis.com/ingress"       = "internal"
#       }
#     }
#   }

#   traffic {
#     percent         = 100
#     latest_revision = true
#   }

#   metadata {
#     labels = {
#       app = "hello-run"
#     }
#   }
#   autogenerate_revision_name = true
# }



resource "google_cloud_run_service" "hello_run" {
  name     = "hello-run"
  location = var.region

  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/hello"
      }
      container_concurrency = 100
      service_account_name  = data.google_service_account.hello_run.email
      timeout_seconds       = (9.5) * 60
    }
    metadata {
      # https://cloud.google.com/run/docs/reference/rest/v1/RevisionTemplate
      annotations = {
        "autoscaling.knative.dev/minScale" = 0
        "autoscaling.knative.dev/maxScale" = 1
        "run.googleapis.com/ingress"       = "all"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  metadata {
    labels = {
      app = "hello-run"
    }
  }
  autogenerate_revision_name = true
}
