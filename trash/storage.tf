###############################################################################
# pharnavaz
###############################################################################

resource "google_storage_bucket" "pharnavaz" {
  name     = "coopnorge-iwana-${var.environment}-pharnavaz"
  location = "europe-north1"
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]
}
