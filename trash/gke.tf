
# https://github.com/dewitt/knative-docs/blob/master/install/Knative-with-GKE.md

resource "google_service_account" "default" {
  account_id   = "service-account-id"
  display_name = "Service Account"
}

locals {
  cluster_secondary_range_name  = "secondary-range-pods"
  services_secondary_range_name = "secondary-range-services"
}





# resource "google_container_cluster" "main" {
#   name     = "main"
#   location = var.region
#   remove_default_node_pool = true
#   initial_node_count       = 1
#   network                  = google_compute_network.gke.self_link
#   subnetwork               = google_compute_subnetwork.gke.self_link
#   ip_allocation_policy {
#     cluster_secondary_range_name  = local.cluster_secondary_range_name
#     services_secondary_range_name = local.services_secondary_range_name
#   }
# }

# resource "google_container_node_pool" "main" {
#   name       = "main"
#   location   = var.region
#   cluster    = google_container_cluster.main.name
#   node_count = 1

#   node_config {
#     preemptible  = true
#     machine_type = "n1-standard-4"

#     # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
#     service_account = google_service_account.default.email
#     oauth_scopes    = [
#       "https://www.googleapis.com/auth/cloud-platform"
#     ]
#   }
# }

# resource "google_compute_network" "gke" {
#   auto_create_subnetworks         = false
#   name                            = "gke"
#   routing_mode                    = "GLOBAL"
# }

# resource "google_compute_subnetwork" "gke" {
#   name          = "gke"
#   ip_cidr_range = "10.132.4.0/22"
#   region        = var.region
#   network       = google_compute_network.gke.id

#   secondary_ip_range {
#     range_name    = local.cluster_secondary_range_name
#     ip_cidr_range = "10.120.0.0/16"
#   }

#   secondary_ip_range {
#     range_name    = local.services_secondary_range_name
#     ip_cidr_range = "10.121.0.0/16"
#   }
# }
