


resource "google_secret_manager_secret" "pim_sp_id" {
  secret_id = "pim-sp-id"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "pim_sp_id" {
  secret      = google_secret_manager_secret.pim_sp_id.id
  secret_data = azuread_service_principal.pim.id
}


resource "google_secret_manager_secret" "pim_sp_b64json" {
  secret_id = "pim-sp-b64json"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "pim_sp_b64json" {
  secret      = google_secret_manager_secret.pim_sp_b64json.id
  secret_data = local.pim_sp_b64json
}


###############################################################################
# Google Cloud Storage Sync
###############################################################################

data "azurerm_storage_account_sas" "pim" {
  connection_string = azurerm_storage_account.pim.primary_connection_string
  https_only        = true
  signed_version    = "2017-07-29"

  resource_types {
    service   = true
    container = true
    object    = true
  }

  services {
    blob  = true
    queue = true
    table = true
    file  = true
  }

  start  = "2022-01-05T00:00:00Z"
  expiry = format("%d-01-01T00:00:00Z", tonumber(formatdate("YYYY", timestamp())) + 2)

  permissions {
    read    = true
    write   = true
    delete  = true
    list    = true
    add     = true
    create  = true
    update  = true
    process = true
  }
}


# https://cloud.google.com/storage-transfer/docs/create-manage-transfer-console#microsoft-azure-blob-storage
# https://cloud.google.com/storage-transfer/docs/reference/rest/v1/transferJobs#TransferJob
resource "google_storage_transfer_job" "pim_output" {
  description = "sync AZFS to GCP"

  transfer_spec {
    object_conditions {
      min_time_elapsed_since_last_modification = "600s"
    }
    transfer_options {
      delete_objects_unique_in_sink              = false
      overwrite_objects_already_existing_in_sink = false
      delete_objects_from_source_after_transfer  = false
    }
    azure_blob_storage_data_source {
      storage_account = azurerm_storage_account.pim.name
      container       = azurerm_storage_container.pim_output.name
      path            = ""
      azure_credentials {
        sas_token = data.azurerm_storage_account_sas.pim.sas
      }
    }
    gcs_data_sink {
      bucket_name = google_storage_bucket.pim_output.name
      path        = ""
    }
  }

  schedule {
    schedule_start_date {
      year  = 2022
      month = 1
      day   = 1
    }
    start_time_of_day {
      hours   = 23
      minutes = 0
      seconds = 0
      nanos   = 0
    }
  }

  depends_on = [google_storage_bucket_iam_member.pim_output]
}




# resource "google_secret_manager_secret_iam_member" "pim_sp_password" {
#   for_each = merge([for item in [
#     {
#       member = "user:ezra.gayon@avensia.com",
#       roles  = ["roles/secretmanager.secretAccessor"],
#     },
#     {
#       member = "clarito.diocton@avensia.com",
#       roles  = ["roles/secretmanager.secretAccessor"],
#     },
#     {
#       member = "espen.langbraten@avensia.com",
#       roles  = ["roles/secretmanager.secretAccessor"],
#     },
#     ] : {
#     for role in item.roles :
#     "${item.member}/${role}" => { member = item.member, role = role }
#     }
#   ]...)

#   secret_id = google_secret_manager_secret.pim_sp_password.id
#   role       = each.value.role
#   member     = each.value.member
# }

# data "azuread_user" "default" {
#   user_principal_name = ""
# }

# resource "azurerm_role_assignment" "pim_password_reader" {
#   scope                = "${data.azurerm_key_vault.default.id}/secrets/${azurerm_key_vault_secret.pim_password.name}"
#   role_definition_name = "Key Vault Secrets User"
#   principal_id         = local.azuread_users["filip1.stawicki@coop.no"].object_id
# }



resource "google_storage_bucket_iam_member" "pim_output" {
  bucket = google_storage_bucket.pim_output.name
  for_each = merge([for item in [
    {
      member = "serviceAccount:${data.google_storage_transfer_project_service_account.default.email}",
      roles  = ["roles/storage.admin"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  role   = each.value.role
  member = each.value.member
}
