###############################################################################
# project level
###############################################################################

resource "google_service_account" "hello_app" {
  account_id   = "hello-app"
  display_name = "hello-app"
}


###############################################################################
# variables
###############################################################################

locals {
  hello_app_sa_id    = "hello-app"
  hello_app_sa_email = "${local.hello_app_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}

###############################################################################
# service account
###############################################################################

data "google_service_account" "hello_app" {
  account_id = local.hello_app_sa_id

  depends_on = [
    google_service_account.hello_app
  ]
}

###############################################################################
# app
###############################################################################

resource "google_app_engine_application" "hello_app" {
  location_id = "europe-west"
}


resource "google_app_engine_flexible_app_version" "hello_app" {
  version_id = "v1"
  service    = "default"
  runtime    = "custom"

  # entrypoint {
  #   shell = "node ./app.js"
  # }

  # deployment {
  #   zip {
  #     source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
  #   }
  # }

  deployment {
    container {
      image = "gcr.io/cloudrun/hello:latest"
    }
  }

  liveness_check {
    path = "/"
  }

  readiness_check {
    path = "/"
  }

  # env_variables = {
  #   PORT = "8080"
  # }

  automatic_scaling {
    cool_down_period = "120s"
    cpu_utilization {
      target_utilization = 0.5
    }
  }

  noop_on_destroy = true
}
