resource "github_repository" "go_masker_lib" {
  name = "go-masker-lib"

  visibility = "public"

  auto_init = true

  has_issues   = true
  has_projects = true
  has_wiki     = true
  is_template  = false

  delete_branch_on_merge = true

  vulnerability_alerts = true
}


# resource "github_branch" "go_masker_lib" {
#   repository = github_repository.go_masker_lib.name
#   branch     = "main"
# }

resource "github_branch_default" "default" {
  repository = github_repository.go_masker_lib.name
  branch     = "main"
}


resource "github_branch_protection" "go_masker_lib" {
  repository_id = github_repository.go_masker_lib.name

  pattern             = github_branch_default.default.branch
  enforce_admins      = false
  allows_force_pushes = true

  required_status_checks {
    strict = false
    # contexts = ["validate/go-version=1.16", "validate/go-version=1.17"]
    contexts = ["build"]
  }

  required_pull_request_reviews {
    dismiss_stale_reviews           = true
    require_code_owner_reviews      = true
    required_approving_review_count = 1
  }
}
