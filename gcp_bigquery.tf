###############################################################################
# pubsub topic
###############################################################################

resource "google_bigquery_dataset" "scratchpad" {
  dataset_id = "scratchpad"
  location   = local.bigquery_location
}
