locals {
  cloud_scheduler_region = "europe-west1"
  cloudbuild_location    = "europe-north1"
  gcs_location           = "europe-north1"
  bigquery_location      = "EU"
}
