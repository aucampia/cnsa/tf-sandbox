variable "gcn_project_id" {
  type = string
}
variable "gcn_network_name" {
  type = string
}
variable "gcn_subnet_name" {
  type = string
}
