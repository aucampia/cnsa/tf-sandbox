data "google_compute_network" "coop" {
  project = var.gcn_project_id
  name    = var.gcn_network_name
}

data "google_compute_subnetwork" "project" {
  project = var.gcn_project_id
  region  = var.region
  name    = var.gcn_subnet_name
}
