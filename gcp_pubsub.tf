###############################################################################
# pubsub topic
###############################################################################

resource "google_pubsub_topic" "scratchpad" {
  name                       = "scratchpad"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "scratchpad_pull" {
  name                       = "scratchpad-pull"
  topic                      = google_pubsub_topic.scratchpad.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  retry_policy {
    minimum_backoff = "1s"
    maximum_backoff = "60s"
  }
  dead_letter_policy {
    dead_letter_topic     = google_pubsub_topic.scratchpad_pull_dl.id
    max_delivery_attempts = 10
  }
}

resource "google_pubsub_topic" "scratchpad_pull_dl" {
  name                       = "scratchpad-pull-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "scratchpad_pull_dl" {
  name                       = "scratchpad-pull-dl"
  topic                      = google_pubsub_topic.scratchpad_pull_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}


# resource "google_project_iam_member" "cloud_builds" {
#   for_each = merge([for item in [
#     {
#       member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudscheduler.iam.gserviceaccount.com"
#       roles = [
#         "roles/pubsub.publisher",
#       ],
#     },
#     # {
#     #   member = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
#     #   roles = [
#     #     "roles/logging.logWriter",
#     #   ]
#     # }
#     ] : {
#     for role in item.roles :
#     "${item.member}/${role}" => { member = item.member, role = role }
#     }
#   ]...)
#   project = data.google_project.project.id
#   role    = each.value.role
#   member  = each.value.member
# }


resource "google_project_iam_member" "pubsub_sa" {
  for_each = merge([for item in(var.environment != "production" ? [
    {
      members = [
        "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com",
      ],
      roles = [
        "roles/bigquery.dataEditor",
        "roles/bigquery.metadataViewer",
        "roles/pubsub.publisher",
        "roles/pubsub.subscriber",
      ],
    },
    ] : []) : {
    for tpl in setproduct(item.members, item.roles) :
    "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
    }
  ]...)
  project = data.google_project.project.id
  member  = each.value.member
  role    = each.value.role
}
