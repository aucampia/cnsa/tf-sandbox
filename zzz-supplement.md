# ...


```bash
gcloud pubsub subscriptions delete --project sandbox-iwan-aucamp-e332 laimahvu-schemaless-bq
gcloud pubsub subscriptions create --project sandbox-iwan-aucamp-e332 laimahvu-schemaless-bq \
  --topic=projects/sandbox-iwan-aucamp-e332/topics/laimahvu-schemaless \
  --bigquery-table=sandbox-iwan-aucamp-e332:laimahvu.laimahvu-schemaless \
  --write-metadata \
  --message-retention-duration=7d \
  --expiration-period=never --ack-deadline=600 \
  --min-retry-delay=10 --max-retry-delay=600 \
  --dead-letter-topic-project=sandbox-iwan-aucamp-e332 \
  --dead-letter-topic=laimahvu-schemaless-bq-dl

```

```
gcloud pubsub subscriptions delete --project sandbox-iwan-aucamp-e332 laimahvu-cloudevent-bq
gcloud pubsub subscriptions create --project sandbox-iwan-aucamp-e332 laimahvu-cloudevent-bq \
  --topic=projects/sandbox-iwan-aucamp-e332/topics/laimahvu-cloudevent \
  --bigquery-table=sandbox-iwan-aucamp-e332:laimahvu.laimahvu-cloudevent \
  --write-metadata --use-topic-schema \
  --message-retention-duration=7d \
  --expiration-period=never --ack-deadline=600 \
  --min-retry-delay=10 --max-retry-delay=600 \
  --dead-letter-topic-project=sandbox-iwan-aucamp-e332 \
  --dead-letter-topic=laimahvu-cloudevent-bq-dl
```

```
protoc --bq-schema_out=spec/proto/bq pubsub_cloudevents_standalone_bq.proto --proto_path=spec/proto/
```


```
gcloud pubsub topics publish projects/sandbox-iwan-aucamp-e332/topics/laimahvu-cloudevent "wrong message"


curl -H "Authorization: Bearer $(gcloud auth application-default print-access-token)"   -H "Content-Type: application/json"   https://pubsub.googleapis.com/v1/projects/sandbox-iwan-aucamp-e332/topics/laimahvu-cloudevent:publish   --data-binary '@/dev/stdin'
```

```bash
bq query --nouse_legacy_sql  --format prettyjson --project_id "sandbox-iwan-aucamp-e332" --dataset_id "laimahvu" '
SELECT * FROM `sandbox-iwan-aucamp-e332.laimahvu.laimahvu`
LIMIT 10
'
```
