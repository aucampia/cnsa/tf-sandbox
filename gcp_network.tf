#tfsec:ignore:google-compute-enable-vpc-flow-logs
module "gcp_network_default" {
  source  = "terraform-google-modules/network/google"
  version = "~> 4.0"

  project_id   = data.google_project.project.project_id
  network_name = "default"
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.132.4.0/24"
      subnet_region = local.gcp_network_region
    },
  ]

  secondary_ranges = {
    subnet-01 = [
      {
        range_name    = "subnet-01-secondary-01"
        ip_cidr_range = "10.120.0.0/24"
      },
    ]
  }
}

data "google_compute_network" "coop" {
  project = var.project_id
  name    = module.gcp_network_default.network_name
}

# Project-scoped subnet
data "google_compute_subnetwork" "project" {
  project = var.project_id
  region  = module.gcp_network_default.subnets["${local.gcp_network_region}/subnet-01"].region
  name    = module.gcp_network_default.subnets["${local.gcp_network_region}/subnet-01"].name
}


resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = data.google_compute_network.coop.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = data.google_compute_network.coop.id
}

// https://cloud.google.com/build/docs/private-pools/set-up-private-pool-environment#gcloud_1

# resource "google_compute_global_address" "cloud_build" {
#   provider = google-beta

#   name          = "cloud-build"
#   purpose       = "VPC_PEERING"
#   address_type  = "INTERNAL"
#   prefix_length = 16
#   network       = data.google_compute_network.coop.id
# }


# resource "google_compute_address" "cloud_build" {
#   name         = "cloud-build"
#   region       = local.gcp_network_region
#   network      = module.gcp_network_default.network.self_link
#   purpose      = "VPC_PEERING"
#   address_type = "INTERNAL"
#   address      = "10.140.4.0/22"
#   # prefix_length = 22

# }

# resource "google_compute_network_peering" "cloud_build" {
#   name = "cloud-build"
#   network
# }


# resource "google_service_networking_connection" "cloud_build" {
#   network                 = module.gcp_network_default.network.id
#   service                 = "servicenetworking.googleapis.com"
#   reserved_peering_ranges = [google_compute_global_address.private_ip_alloc.name]
# }
