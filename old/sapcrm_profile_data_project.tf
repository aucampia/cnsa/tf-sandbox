###############################################################################
# variables
###############################################################################

locals {
  # sapcrm_event_handler_sa_id    = "sapcrm-event-handler"
  # sapcrm_event_handler_sa_email = "${local.sapcrm_event_handler_sa_id}@${var.project_id}.iam.gserviceaccount.com"
  sapcrm_dump_handler_sa_id    = "sapcrm-dump-handler"
  sapcrm_dump_handler_sa_email = "${local.sapcrm_dump_handler_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}


resource "google_service_account" "sapcrm_event_handler" {
  account_id = local.sapcrm_event_handler_sa_id
}

resource "google_service_account" "sapcrm_dump_handler" {
  account_id = local.sapcrm_dump_handler_sa_id
}
