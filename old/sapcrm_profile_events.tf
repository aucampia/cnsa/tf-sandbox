###############################################################################
# variables
###############################################################################

locals {
  sapcrm_event_handler_sa_id    = "sapcrm-event-handler"
  sapcrm_event_handler_sa_email = "${local.sapcrm_event_handler_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}

###############################################################################
# pubsub topic
###############################################################################

resource "google_pubsub_topic" "sapcrm_profile_events" {
  name                       = "sapcrm-profile-events"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "sapcrm_profile_events" {
  name                       = "sapcrm-profile-events"
  topic                      = google_pubsub_topic.sapcrm_profile_events.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_topic_iam_member" "sapcrm_profile_events" {
  for_each = merge([for item in(var.environment != "production" ? [
    {
      members = [
        "serviceAccount:${local.sapcrm_event_handler_sa_email}",
      ],
      roles = [
        "roles/pubsub.publisher",
        "roles/pubsub.viewer",
      ],
    },
    ] : []) : {
    for tpl in setproduct(item.members, item.roles) :
    "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
    }
  ]...)
  member = each.value.member
  role   = each.value.role
  topic  = google_pubsub_topic.sapcrm_profile_events.name
}


###############################################################################
# bigquery
###############################################################################

resource "google_bigquery_dataset" "sapcrm_profile_events" {
  dataset_id = "sapcrm_profile_events"
  location   = local.bigquery_location
}

resource "google_bigquery_dataset_iam_member" "sapcrm_profile_events" {
  for_each = merge([for item in(var.environment != "production" ? [
    {
      members = [
        "serviceAccount:${local.sapcrm_event_handler_sa_email}",
      ],
      roles = [
        "roles/bigquery.admin",
      ],
    },
    ] : []) : {
    for tpl in setproduct(item.members, item.roles) :
    "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
    }
  ]...)
  member     = each.value.member
  role       = each.value.role
  dataset_id = google_bigquery_dataset.sapcrm_profile_events.dataset_id
}

# resource "google_bigquery_table" "sapcrm_profile_events" {
#   dataset_id          = google_bigquery_dataset.sapcrm_profile_events.dataset_id
#   table_id            = "sapcrm_profile_events"
#   deletion_protection = false
#   schema = file("schema/sap_pmr/sap_pmr_offers.json")
#   time_partitioning {
#     expiration_ms            = 90 * 24 * 60 * 60 * 1000 # 90 days
#     field                    = "time"
#     type                     = "DAY"
#     require_partition_filter = true
#   }
# }
