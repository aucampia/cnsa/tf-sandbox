###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "products_information" {
  account_id   = local.products_information_sa_id
  display_name = local.products_information_sa_id
}

# This file contains things specific to the Product Information Service.

locals {
  products_information_sa_id = "products-information"
}

###############################################################################
# GCP Service Account
###############################################################################

data "google_service_account" "products_information" {
  account_id = local.products_information_sa_id
}

###############################################################################
# Database
###############################################################################

locals {
  # https://cloud.google.com/sql/docs/postgres/instance-settings
  google_sql_database_instance_products_information_options = lookup({
    production = {
      tier      = "db-custom-1-3840"
      disk_type = "PD_SSD"
    }
    staging = {
      tier      = "db-custom-1-3840"
      disk_type = "PD_SSD"
    }
    test = {
      tier      = "db-f1-micro"
      disk_type = "PD_HDD"
    }
    dev = {
      tier      = "db-f1-micro"
      disk_type = "PD_HDD"
    }
    iaucsb = {
      tier      = "db-f1-micro"
      disk_type = "PD_HDD"
    }
  }, var.environment, {})
}

resource "google_sql_database_instance" "products_information" {
  name                = "products-information"
  database_version    = "POSTGRES_14"
  region              = var.region
  deletion_protection = false

  settings {
    tier      = local.google_sql_database_instance_products_information_options.tier
    disk_type = local.google_sql_database_instance_products_information_options.disk_type
    ip_configuration {
      ipv4_enabled    = false
      private_network = data.google_compute_network.coop.id
      require_ssl     = true
    }
    insights_config {
      query_insights_enabled = true
    }
    backup_configuration {
      enabled = true
    }
    database_flags {
      name  = "log_temp_files"
      value = "0"
    }
    database_flags {
      name  = "log_connections"
      value = "on"
    }
    database_flags {
      name  = "log_disconnections"
      value = "on"
    }
    database_flags {
      name  = "log_checkpoints"
      value = "on"
    }
    database_flags {
      name  = "log_lock_waits"
      value = "on"
    }
  }

  depends_on = [google_service_networking_connection.private_vpc_connection]
}

resource "google_sql_database" "products_information_products" {
  name     = "products"
  instance = google_sql_database_instance.products_information.name
}


resource "random_password" "products_information_service_db" {
  length  = 32
  special = false
}

resource "google_secret_manager_secret" "products_information_service_db" {
  secret_id = "products_information_service_db"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "products_information_service_db" {
  secret      = google_secret_manager_secret.products_information_service_db.id
  secret_data = random_password.products_information_service_db.result
}

resource "google_secret_manager_secret_iam_member" "products_information_service" {
  secret_id = google_secret_manager_secret.products_information_service_db.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${data.google_service_account.products_information.email}"
}


resource "google_sql_user" "products_information_service" {
  name     = "product-information-service"
  instance = google_sql_database_instance.products_information.name
  password = random_password.products_information_service_db.result
}

###############################################################################
# Google Cloud Storage
###############################################################################

# Bucket for processed PIM files
resource "google_storage_bucket" "pim_product_archive" {
  name     = "coopnorge-products-${var.environment}-pim-archive"
  location = local.gcs_location
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  uniform_bucket_level_access = true

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]

  lifecycle_rule {
    condition {
      age = 90
    }
    action {
      type          = "SetStorageClass"
      storage_class = "ARCHIVE"
    }
  }
}

resource "google_storage_bucket_iam_member" "products_information_service" {
  for_each = { for item in [
    {
      bucket = google_storage_bucket.pim_product_archive.name,
      role   = "roles/storage.admin",
    },
    {
      bucket = google_storage_bucket.pim_product_dumps.name,
      role   = "roles/storage.admin",
    },
    ] :
    "${item.bucket}/${item.role}" => item
  }
  bucket = each.value.bucket
  role   = each.value.role
  member = "serviceAccount:${data.google_service_account.products_information.email}"
}
