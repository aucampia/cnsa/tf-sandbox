data "azuread_service_principal" "pim_syncer" {
  display_name = "urn:fdc:coop.no:20220105:products:pim-syncer:${var.environment}"
}

data "azuread_service_principal" "pim_syncer_production" {
  display_name = "urn:fdc:coop.no:20220105:products:pim-syncer:production"
}

data "azuread_service_principal" "pim_syncer_staging" {
  display_name = "urn:fdc:coop.no:20220105:products:pim-syncer:staging"
}


# ###############################################################################
# # Azure Service Principal
# ###############################################################################

# resource "azuread_application" "pim" {
#   display_name    = "pim"
#   identifier_uris = ["urn:fdc:coop.no:20220105:products:pim:${var.environment}"]
# }

# resource "azuread_service_principal" "pim" {
#   application_id               = azuread_application.pim.application_id
#   app_role_assignment_required = false
# }

# resource "azuread_service_principal_password" "pim" {
#   service_principal_id = azuread_service_principal.pim.id
# }

# locals {
#   # https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli#1-create-a-service-principal
#   # https://docs.microsoft.com/en-us/azure/aks/kubernetes-service-principal?tabs=azure-cli#manually-create-a-service-principal
#   # az ad sp show --id a538d577-d677-4bf1-87de-6f6124fd1b4a
#   pim_sp_obj = {
#     "appId" : azuread_service_principal.pim.application_id,
#     "displayName" : azuread_service_principal.pim.display_name,
#     "name" : one(azuread_application.pim.identifier_uris),
#     "password" : azuread_service_principal_password.pim.value,
#     "tenant" : azuread_service_principal.pim.application_tenant_id
#   }
#   pim_sp_json    = jsonencode(local.pim_sp_obj)
#   pim_sp_b64json = base64encode(local.pim_sp_json)
# }

# resource "azurerm_key_vault_secret" "pim_sp_password" {
#   key_vault_id = data.azurerm_key_vault.default.id
#   name         = "pim-sp-password"
#   value        = azuread_service_principal_password.pim.value
# }

# resource "azurerm_key_vault_secret" "pim_sp_json" {
#   key_vault_id = data.azurerm_key_vault.default.id
#   name         = "pim-sp-json"
#   value        = local.pim_sp_json
# }

# resource "google_secret_manager_secret" "pim_sp_password" {
#   secret_id = "pim-sp-password"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "pim_sp_password" {
#   secret      = google_secret_manager_secret.pim_sp_password.id
#   secret_data = azuread_service_principal_password.pim.value
# }

# resource "google_secret_manager_secret" "pim_sp_json" {
#   secret_id = "pim-sp-json"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "pim_sp_json" {
#   secret      = google_secret_manager_secret.pim_sp_json.id
#   secret_data = local.pim_sp_json
# }


###############################################################################
# Azure Storage
###############################################################################

resource "azurerm_storage_account" "pim" {
  #tfsec:ignore:AZU015
  name                = "cnsairpim${var.environment}"
  resource_group_name = var.rg_name
  location            = var.location

  # account_kind             = "StorageV2"
  # account_tier             = "Standard"
  # account_replication_type = "RAGRS"

  # min_tls_version          = "TLS1_2"
  account_kind             = "StorageV2"
  account_tier             = "Standard"
  account_replication_type = "RAGRS"
  access_tier              = "Hot"
}

# Bucket for output from PIM
resource "azurerm_storage_container" "product_dumps" {
  name                  = "product-dumps"
  storage_account_name  = azurerm_storage_account.pim.name
  container_access_type = "private"
}

# resource "azurerm_role_assignment" "pim_output_pim" {
#   scope                = azurerm_storage_container.pim_output.resource_manager_id


#   role_definition_name = "Storage Blob Data Contributor"
#   principal_id         = azuread_service_principal.pim.id
# }

# resource "azurerm_role_assignment" "pim_output_pim_syncer" {
#   scope                = azurerm_storage_container.product_dumps.resource_manager_id
#   role_definition_name = "Storage Blob Data Contributor"
#   principal_id         = azuread_service_principal.pim_syncer.id
# }


resource "azurerm_role_assignment" "pim_syncer" {
  for_each = toset([
    "Storage Blob Data Contributor",
  ])
  scope                = azurerm_storage_account.pim.id
  role_definition_name = each.value
  # This is from https://terraform.coop.no/app/coopnorge/workspaces/products-production
  principal_id = data.azuread_service_principal.pim_syncer.object_id
}

# module "pimstorage" {
#   source = "./terraform-azure-storageaccount"

#   # source              = "terraform.coop.no/coopnorge/storageaccount/azure"
#   # version             = "~> 0.2.2"
#   environment         = var.environment
#   location            = var.location
#   name                = "pimstorage"
#   resource_group_name = azurerm_resource_group.default.name
#   kind                = "StorageV2"
#   shares = [
#     # {
#     #   name  = "pimstorage"
#     #   quota = 5000
#     # }
#   ]
#   containers = [
#     {
#       name        = "product-dumps"
#       access_type = "private"
#     }
#   ]
#   network_rules = {
#     ip_rules = ["62.92.133.5", "52.232.73.67", "217.115.58.195", "83.233.241.34", "82.209.162.218", "103.44.235.70", "37.221.231.158", "37.221.226.178",
#       # GCP VPC: https://dev.azure.com/coopnorge/Engineering/_wiki/wikis/Engineering.wiki/1142/Network?anchor=cloud-nat
#       "35.228.83.11", "34.77.80.161",
#     ]
#     subnet_ids = []
#     bypass     = ["AzureServices"]
#   }
# }

locals {
  firewall_subnet_id = ["/subscriptions/755d844e-3bc4-41fe-8027-1fda4f5b7b2c/resourceGroups/rg-expressroute/providers/Microsoft.Network/virtualNetworks/vnet-expressroute/subnets/AzureFirewallSubnet"]
  tfe_subnet_id      = ["/subscriptions/63bdfd44-71e0-4d26-946f-1c9bb12e7dd3/resourceGroups/rg-network/providers/Microsoft.Network/virtualNetworks/vnet-tf-enterprise-production/subnets/tf-enterprise-subnet-0"]
}

resource "azurerm_storage_account" "pimx" {
  name                = "iedahpim${var.environment}"
  resource_group_name = var.rg_name
  location            = var.location
  # min_tls_version           = "TLS1_3"
  account_kind              = "StorageV2"
  account_tier              = "Standard"
  account_replication_type  = "RAGRS"
  access_tier               = "Hot"
  enable_https_traffic_only = true

  identity {
    type = "SystemAssigned"
  }

  network_rules {
    default_action = "Deny"
    ip_rules = ["62.92.133.5", "52.232.73.67", "217.115.58.195", "83.233.241.34", "82.209.162.218", "103.44.235.70", "37.221.231.158", "37.221.226.178",
      # GCP VPC: https://dev.azure.com/coopnorge/Engineering/_wiki/wikis/Engineering.wiki/1142/Network?anchor=cloud-nat
      "35.228.83.11", "34.77.80.161",
      "88.91.243.38",
    ]
    virtual_network_subnet_ids = var.location == "norwayeast" ? concat(local.firewall_subnet_id, []) : concat(local.tfe_subnet_id, [])
    bypass                     = ["AzureServices"]
  }

}

resource "azurerm_storage_container" "pimx_product_dumps" {
  name                  = "product-dumps"
  storage_account_name  = azurerm_storage_account.pimx.name
  container_access_type = "private"
}




resource "azurerm_role_assignment" "pimx_syncer" {
  for_each = toset([
    "Reader and Data Access",
    "Storage Blob Data Reader",
    "Storage Blob Data Contributor",
    "Contributor",
  ])
  scope                = azurerm_storage_account.pimx.id
  role_definition_name = each.value
  # This is from https://terraform.coop.no/app/coopnorge/workspaces/products-production
  principal_id = data.azuread_service_principal.pim_syncer.object_id
}

resource "azurerm_role_assignment" "pimx_syncer_staging" {
  for_each = toset([
    "Reader and Data Access",
    "Storage Blob Data Reader",
    "Storage Blob Data Contributor",
    "Contributor",
  ])
  scope                = azurerm_storage_account.pimx.id
  role_definition_name = each.value
  # This is from https://terraform.coop.no/app/coopnorge/workspaces/products-production
  principal_id = data.azuread_service_principal.pim_syncer_staging.object_id
}
