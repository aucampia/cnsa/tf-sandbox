###############################################################################
# service account
###############################################################################

resource "google_service_account" "sapcrm_event_dispatcher" {
  account_id = "sapcrm-event-dispatcher"
}

resource "google_service_account_key" "sapcrm_event_dispatcher" {
  service_account_id = google_service_account.sapcrm_event_dispatcher.id
}

resource "google_secret_manager_secret" "sapcrm_event_dispatcher_sak" {
  secret_id = "sapcrm-event-dispatcher-sak"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "sapcrm_event_dispatcher_sak" {
  secret      = google_secret_manager_secret.sapcrm_event_dispatcher_sak.id
  secret_data = base64decode(google_service_account_key.sapcrm_event_dispatcher.private_key)
}

resource "google_secret_manager_secret_iam_member" "sapcrm_event_dispatcher_sak" {
  for_each = merge([for item in [
    {
      members = concat([
        "user:christian.stemmer@coop.no",
      ]),
      roles = [
        "roles/secretmanager.secretAccessor",
        "roles/secretmanager.viewer",
      ],
    },
    ] : {
    for tpl in setproduct(item.members, item.roles) :
    "${tpl[0]}-${tpl[1]}" => { member = tpl[0], role = tpl[1], }
    }
  ]...)
  member    = each.value.member
  role      = each.value.role
  secret_id = google_secret_manager_secret.sapcrm_event_dispatcher_sak.id
}


