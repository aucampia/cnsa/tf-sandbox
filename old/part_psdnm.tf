resource "google_secret_manager_secret" "file_psdnmsr_salt" {
  secret_id = "file_psdnmsr_salt"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "random_password" "file_psdnmsr_salt" {
  length  = 32
  special = false
}

resource "google_secret_manager_secret_version" "file_psdnmsr_salt" {
  secret      = google_secret_manager_secret.file_psdnmsr_salt.id
  secret_data = random_password.file_psdnmsr_salt.result
}

resource "google_storage_bucket" "pim_product_dumps" {
  name     = "coopnorge-psndm-${var.environment}-file-psdnm-cfg-dh"
  location = local.gcs_location
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]

  lifecycle_rule {
    condition {
      age = 90
    }
    action {
      type = "Delete"
    }
  }
}
