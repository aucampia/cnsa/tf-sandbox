locals {
  sap_sftp_username = lookup({
    "production" : "ftpcdtdp",
    "staging" : "ftpcdtdt",
    "test" : "ftpcdtdt",
    "dev" : "ftpcdtdt",
  }, var.environment, "ftpcdtds")
  sap_sftp_hostname = lookup({
    "production" : "sapbtsp.coop.no",
    "staging" : "sapbtst.coop.no",
    "test" : "sapbtst.coop.no",
    "dev" : "sapbtst.coop.no",
  }, var.environment, "sapbtss.coop.no")
  sap_sftp_ip = lookup({
    "production" : "10.193.115.188",
    "staging" : "10.193.113.27",
    "test" : "10.193.113.27",
    "dev" : "10.193.113.27",
  }, var.environment, "sapbtss.coop.no")
  sap_sftp_sid = lookup({
    "production" : "CP1",
    "staging" : "CQ2",
    "test" : "CT1",
    "dev" : "CS1",
  }, var.environment, "sapbtss.coop.no")
  sap_sftp_path_prefix     = "/usr/sap/common/customer_data_domain"
  sap_sftp_path_env_prefix = "${local.sap_sftp_path_prefix}/${local.sap_sftp_sid}"
}

resource "google_compute_project_metadata_item" "sap_sftp_username" {
  key   = "sap-sftp-username"
  value = local.sap_sftp_username
}

resource "google_secret_manager_secret" "sap_sftp_password" {
  secret_id = "sap_sftp_password"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "google_secret_manager_secret_iam_member" "sap_sftp_password" {
  for_each = { for value in setproduct(
    [
      "user:martin.sporsheim@coop.no",
      "user:thomas.schmidtke@coop.no",
      "user:iwan.aucamp@coop.no",
    ],
    [
      "roles/secretmanager.secretVersionAdder",
      "roles/secretmanager.viewer",
    ]
  ) : "${value[0]}-${value[1]}" => value }
  secret_id = google_secret_manager_secret.sap_sftp_password.id
  role      = each.value[1]
  member    = each.value[0]
}

## LOCAL ONLY

resource "google_secret_manager_secret_version" "sap_sftp_password" {
  secret      = google_secret_manager_secret.sap_sftp_password.id
  secret_data = "initial"
  lifecycle {
    ignore_changes = [
      secret_data
    ]
  }
}
