# This file contains things specific to the InRiver PIM system.

###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "pim_syncer" {
  account_id   = "pim-syncer"
  display_name = "pim-syncer"
}

resource "google_project_iam_member" "sas" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:${google_service_account.pim_syncer.email}"
      roles = [
        "roles/logging.logWriter",
        # "roles/cloudbuild.serviceAgent",
      ],
    }
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  project = data.google_project.project.id
  role    = each.value.role
  member  = each.value.member
  depends_on = [
    google_service_account.pim_syncer
  ]
}

###############################################################################
# Variables
###############################################################################

locals {
  # pim_syncer_sa_email = "pim-syncer@${var.project_id}.iam.gserviceaccount.com"
  pim_azure_storage_account = lookup({
    "production" : "pimstorageproduction",
    "staging" : "pimstoragestaging",
    "iaucsb" : "iedahpim${var.environment}",
    # "iaucsb" : "cnsairpim${var.environment}",
  }, var.environment, null)
  pim_product_dumps_azure_storage_container = "product-dumps"
  pim_syncer_sa_id                          = "pim-syncer"
}

###############################################################################
# GCP Service Account
###############################################################################

data "google_service_account" "pim_syncer" {
  account_id = local.pim_syncer_sa_id

  depends_on = [
    google_service_account.pim_syncer
  ]
}

###############################################################################
# Google Cloud Storage
###############################################################################

# Bucket for files from PIM
resource "google_storage_bucket" "pim_product_dumps" {
  name                        = "coopnorge-products-${var.environment}-pim-output"
  location                    = local.gcs_location
  uniform_bucket_level_access = true
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]

  lifecycle_rule {
    condition {
      age = 90
    }
    action {
      type          = "SetStorageClass"
      storage_class = "ARCHIVE"
    }
  }
}

resource "google_storage_bucket_iam_member" "pim_product_dumps" {
  for_each = { for item in [
    {
      member = "serviceAccount:${data.google_service_account.pim_syncer.email}",
      role   = "roles/storage.admin",
    },
    ] :
    "${item.member}/${item.role}" => item
  }
  bucket = google_storage_bucket.pim_product_dumps.name
  role   = each.value.role
  member = each.value.member
}




# ###############################################################################
# # Azure Service Principal
# ###############################################################################

# locals {
#   # https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli#1-create-a-service-principal
#   # https://docs.microsoft.com/en-us/azure/aks/kubernetes-service-principal?tabs=azure-cli#manually-create-a-service-principal
#   # az ad sp show --id a538d577-d677-4bf1-87de-6f6124fd1b4a
#   pim_syncer_sp_uri = "urn:fdc:coop.no:20220105:products:pim-syncer:${var.environment}"
#   pim_syncer_sp_obj = {
#     "appId" : azuread_service_principal.pim_syncer.application_id,
#     "displayName" : azuread_service_principal.pim_syncer.display_name,
#     "name" : one(azuread_application.pim_syncer.identifier_uris),
#     "password" : azuread_service_principal_password.pim_syncer.value,
#     "tenant" : azuread_service_principal.pim_syncer.application_tenant_id
#   }
#   pim_syncer_sp_json    = jsonencode(local.pim_syncer_sp_obj)
#   pim_syncer_sp_b64json = base64encode(local.pim_syncer_sp_json)
# }

# resource "azuread_application" "pim_syncer" {
#   display_name    = local.pim_syncer_sp_uri
#   identifier_uris = [local.pim_syncer_sp_uri]
# }

# resource "azuread_service_principal" "pim_syncer" {
#   application_id               = azuread_application.pim_syncer.application_id
#   app_role_assignment_required = false
# }

# resource "azuread_service_principal_password" "pim_syncer" {
#   service_principal_id = azuread_service_principal.pim_syncer.id
# }


# resource "google_secret_manager_secret" "pim_syncer_sp_password" {
#   secret_id = "pim-syncer-sp-password"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# # resource "google_secret_manager_secret_version" "pim_syncer_sp_password" {
# #   secret      = google_secret_manager_secret.pim_syncer_sp_password.id
# #   secret_data = azuread_service_principal_password.pim_syncer.value
# # }

# resource "google_secret_manager_secret" "pim_syncer_sp_json" {
#   secret_id = "pim-syncer-sp-json"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_secret_manager_secret_version" "pim_syncer_sp_json" {
#   secret      = google_secret_manager_secret.pim_syncer_sp_json.id
#   secret_data = local.pim_syncer_sp_json
# }

# resource "google_secret_manager_secret_iam_member" "pim_syncer_sp_json" {
#   secret_id = google_secret_manager_secret.pim_syncer_sp_json.id
#   role      = "roles/secretmanager.secretAccessor"
#   member    = "serviceAccount:${data.google_service_account.pim_syncer.email}"
# }

# ###############################################################################
# # Cloud Build
# ###############################################################################

# # https://cloud.google.com/logging/docs/buckets
# # resource "google_storage_bucket_iam_member" "pim_syncer_cb_logs" {
# #   bucket = google_storage_bucket.cloud_build_logs.name
# #   role   = "roles/storage.admin"
# #   member = "serviceAccount:${data.google_service_account.pim_syncer.email}"
# # }

# resource "google_pubsub_topic" "pim_syncer" {
#   name                       = "pim-syncer"
#   message_retention_duration = "${7 * 24 * 60 * 60}s"
# }

# resource "google_pubsub_subscription" "pim_syncer" {
#   name                       = "pim-syncer"
#   topic                      = google_pubsub_topic.pim_syncer.id
#   ack_deadline_seconds       = 600
#   message_retention_duration = "${7 * 24 * 60 * 60}s"
#   labels = {
#     app = "pim-syncer"
#   }
# }

# resource "google_cloud_scheduler_job" "pim_syncer" {
#   # region      = "europe-central2"
#   region      = "europe-west1"
#   name        = "pim-syncer"
#   description = "pim-syncer"
#   schedule    = "0 0 1 1 0"

#   pubsub_target {
#     topic_name = google_pubsub_topic.pim_syncer.id
#     attributes = {
#       action = "run"
#     }
#   }
# }


# # gcloud alpha builds triggers run --project sandbox-iwana-sandbox-f06a pim-syncer

# # https://cloud.google.com/build/docs/build-config-file-schema
# # https://cloud.google.com/build/docs/api/reference/rest/v1/projects.builds#Build.LoggingMode
# resource "google_cloudbuild_trigger" "pim_syncer" {
#   name = "pim-syncer"

#   pubsub_config {
#     topic                 = google_pubsub_topic.pim_syncer.id
#     service_account_email = data.google_service_account.pim_syncer.email
#   }
#   service_account = data.google_service_account.pim_syncer.id

#   # https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values
#   # https://cloud.google.com/build/docs/configuring-builds/use-bash-and-bindings-in-substitutions
#   substitutions = {
#     # This is the pubsub message body.
#     # _PUBSUB_MESSAGE_BODY = "$(body)"
#   }

#   build {
#     step {
#       name       = "docker.io/rclone/rclone:1@sha256:1e6eeabddc013302793aad1986af0b8929bb42bb7c616b10a9b66b2ce04e165f"
#       entrypoint = "sh"
#       args = ["-euo", "pipefail", "-c",
#         <<-EOT
#         export RCLONE_CONFIG_SRC_SERVICE_PRINCIPAL_FILE=$(mktemp)
#         printenv SERVICE_PRINCIPAL_JSON > $$${RCLONE_CONFIG_SRC_SERVICE_PRINCIPAL_FILE}
#         SERVICE_PRINCIPAL_JSON=""
#         rclone ls dst:${google_storage_bucket.pim_product_dumps.name}/
#         rclone touch dst:${google_storage_bucket.pim_product_dumps.name}/src/$(date +%Y%m%d%H%M%S).file
#         rclone move dst:${google_storage_bucket.pim_product_dumps.name}/src/ dst:${google_storage_bucket.pim_product_dumps.name}/dst/
#         EOT
#       ]
#       env = [
#         # "PUBSUB_MESSAGE_BODY=$_PUBSUB_MESSAGE_BODY",
#         "RCLONE_USE_JSON_LOG=true",
#         "RCLONE_LOG_FORMAT=date,time,microseconds,pid,longfile,shortfile,UTC",
#         "RCLONE_LOG_LEVEL=INFO",
#         "RCLONE_STATS=5s",
#         "RCLONE_CHECKSUM=true",
#         "RCLONE_STATS_ONE_LINE_DATE=true",
#         "RCLONE_CONFIG_SRC_TYPE=azureblob",
#         "RCLONE_CONFIG_SRC_ACCOUNT=${local.pim_azure_storage_account}",
#         "RCLONE_CONFIG_DST_TYPE=gcs",
#         "RCLONE_CONFIG_DST_GCS_PROJECT_NUMBER=${data.google_project.project.number}",
#         "RCLONE_CONFIG_DST_GCS_LOCATION=${local.gcs_location}",
#       ]
#       secret_env = ["SERVICE_PRINCIPAL_JSON"]
#     }
#     available_secrets {
#       secret_manager {
#         env          = "SERVICE_PRINCIPAL_JSON"
#         version_name = "${google_secret_manager_secret.pim_syncer_sp_json.id}/versions/latest"
#       }
#     }
#     # logs_bucket = google_storage_bucket.cloud_build_logs.name
#     options {
#       logging = "STACKDRIVER_ONLY"
#     }
#   }
# }

