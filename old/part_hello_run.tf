###############################################################################
# project level
###############################################################################

resource "google_service_account" "hello_run" {
  account_id   = "hello-run"
  display_name = "hello-run"
}


###############################################################################
# variables
###############################################################################

locals {
  hello_run_sa_id    = "hello-run"
  hello_run_sa_email = "${local.hello_run_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}

###############################################################################
# service account
###############################################################################

data "google_service_account" "hello_run" {
  account_id = local.hello_run_sa_id

  depends_on = [
    google_service_account.hello_run
  ]
}

###############################################################################
# cloud run
###############################################################################

resource "google_cloud_run_service" "hello_run" {
  name     = "hello-run"
  location = var.region

  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/hello:latest"
      }
      container_concurrency = 100
      service_account_name  = data.google_service_account.hello_run.email
      timeout_seconds       = (9.5) * 60
    }
    metadata {
      # https://cloud.google.com/run/docs/reference/rest/v1/RevisionTemplate
      annotations = {
        "autoscaling.knative.dev/minScale" = 0
        "autoscaling.knative.dev/maxScale" = 1
        "run.googleapis.com/ingress"       = "all"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  metadata {
    labels = {
      app = "hello-run"
    }
  }
  autogenerate_revision_name = true
}

# resource "google_cloud_run_service_iam_member" "hello_run" {
#   location = google_cloud_run_service.hello_run.location
#   project  = google_cloud_run_service.hello_run.project
#   service  = google_cloud_run_service.hello_run.name

#   role   = "roles/run.invoker"
#   member = "allUsers"
# }

# data "google_iam_policy" "noauth" {
#   binding {
#     role = "roles/run.invoker"
#     members = [
#       "allUsers",
#     ]
#   }
# }

# # Enable public access on Cloud Run service
# resource "google_cloud_run_service_iam_policy" "noauth" {
#   location = google_cloud_run_service.hello_run.location
#   project = google_cloud_run_service.hello_run.project
#   service = google_cloud_run_service.hello_run.name
#   policy_data = data.google_iam_policy.noauth.policy_data
# }

# resource "google_project_organization_policy" "allowed_policy_member_domains" {
#   project    = var.project_id
#   constraint = "iam.allowedPolicyMemberDomains"
#   list_policy {
#     allow {
#       all = true
#     }
#   }
# }


resource "google_compute_region_network_endpoint_group" "hello_run" {
  name                  = "hello-run"
  network_endpoint_type = "SERVERLESS"
  region                = var.region
  cloud_run {
    service = google_cloud_run_service.hello_run.name
  }
}

module "gcp_lb_hello_run" {
  source  = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version = "6.2.0"

  project = var.project_id

  name = "hello-run"

  ssl                             = false
  managed_ssl_certificate_domains = []
  https_redirect                  = false
  backends = {
    default = {
      description             = null
      enable_cdn              = false
      custom_request_headers  = null
      custom_response_headers = null
      security_policy         = null


      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      groups = [
        {
          group = google_compute_region_network_endpoint_group.hello_run.id
        }
      ]

      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
    }
  }
}

output "url" {
  value = "http://${module.gcp_lb_hello_run.external_ip}"
}
