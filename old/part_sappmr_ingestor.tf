
###############################################################################
# service account
###############################################################################

# https://cloud.google.com/dataflow/docs/concepts/security-and-permissions
resource "google_service_account" "sappmr_ingestor" {
  account_id   = "sappmr-ingestor"
  display_name = "SAP PMR Ingestor"
}

resource "google_service_account" "sappmr_ingestor_cd" {
  account_id   = "sappmr-ingestor-cd"
  display_name = "SAP PMR Ingestor Continous Deployment"
}

# data "null_data_source" "sappmr_ingestor" {
#   inputs = {
#     sa_email = google_service_account.sappmr_ingestor.email
#     cd_sa_email = google_service_account.sappmr_ingestor_cd.email
#   }
# }
# data.null_data_source.sappmr_ingestor.outputs.sa_email


###############################################################################
# storage
###############################################################################

resource "google_storage_bucket" "sappmr_input" {
  name     = "coopnorge-iwana-${var.environment}-sappmr-input"
  location = var.region
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]
}

resource "google_storage_notification" "sappmr_input_finalize" {
  bucket         = google_storage_bucket.sappmr_input.name
  payload_format = "JSON_API_V1"
  event_types    = ["OBJECT_FINALIZE"]
  topic          = google_pubsub_topic.sappmr_input_finalize.id
  depends_on = [
    google_project_iam_member.sappmr_ingestor
  ]
}

resource "google_storage_bucket_iam_member" "sappmr_ingestor_input" {
  bucket = google_storage_bucket.sappmr_input.name
  role   = "roles/storage.objectViewer"
  member = "serviceAccount:${local.sappmr_ingestor_email}"
}

###############################################################################
# project IAM
###############################################################################

resource "google_project_iam_member" "sappmr_ingestor" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sappmr_ingestor_email}",
      roles = [
        "roles/bigquery.jobUser",
      ],
    },
    {
      member = "serviceAccount:${local.sappmr_ingestor_cd_email}",
      roles = [
        "roles/run.developer",
      ],
    },
    {
      member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
      roles  = ["roles/pubsub.publisher", "roles/pubsub.subscriber"]
    },
    {
      member = "serviceAccount:service-${data.google_project.project.number}@gs-project-accounts.iam.gserviceaccount.com",
      roles  = ["roles/pubsub.publisher"]
    }
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  project    = data.google_project.project.id
  role       = each.value.role
  member     = each.value.member
  depends_on = [google_service_account.sappmr_ingestor]
}


###############################################################################
# variables
###############################################################################

locals {
  # sappmr_ingestor_email = google_service_account.sappmr_ingestor.email
  # sappmr_ingestor_cd_email = google_service_account.sappmr_ingestor_cd.email
  sappmr_ingestor_email    = "sappmr-ingestor@${var.project_id}.iam.gserviceaccount.com"
  sappmr_ingestor_cd_email = "sappmr-ingestor-cd@${var.project_id}.iam.gserviceaccount.com"
}

###############################################################################
# service account IAM
###############################################################################

data "google_service_account" "sappmr_ingestor" {
  account_id = local.sappmr_ingestor_email
}

resource "google_service_account_iam_member" "sappmr_ingestor_cd" {
  service_account_id = data.google_service_account.sappmr_ingestor.name
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sappmr_ingestor_cd_email}",
      roles  = ["roles/iam.serviceAccountUser"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  role   = each.value.role
  member = each.value.member
}

###############################################################################
# pubsub topic
###############################################################################

resource "google_pubsub_topic" "sappmr_input_finalize" {
  name                       = "sappmr-input-finalize"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}


resource "google_pubsub_subscription" "sappmr_input_finalize" {
  name                       = "sappmr-input-finalize"
  topic                      = google_pubsub_topic.sappmr_input_finalize.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

###############################################################################
# pubsub subscription
###############################################################################

resource "google_pubsub_topic" "sappmr_ingestor_input_dl" {
  name                       = "sappmr-ingestor-input-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  labels = {
    app = "sappmr-ingestor"
  }
}

resource "google_pubsub_subscription" "sappmr_ingestor_input_dl" {
  name                       = "sappmr-ingestor-input-dl"
  topic                      = google_pubsub_topic.sappmr_ingestor_input_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  labels = {
    app = "sappmr-ingestor"
  }
}

resource "google_pubsub_subscription" "sappmr_ingestor_input" {
  name                       = "sappmr-ingestor-input"
  topic                      = google_pubsub_topic.sappmr_input_finalize.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${1 * 24 * 60 * 60}s"
  labels = {
    app = "sappmr-ingestor"
  }
  push_config {
    push_endpoint = "${google_cloud_run_service.sappmr_ingestor.status[0].url}/event/pubsub/storage"

    oidc_token {
      service_account_email = local.sappmr_ingestor_email
    }
  }
  retry_policy {
    minimum_backoff = "60s"
    maximum_backoff = "600s"
  }
  dead_letter_policy {
    dead_letter_topic     = google_pubsub_topic.sappmr_ingestor_input_dl.id
    max_delivery_attempts = 5
  }
}

###############################################################################
# bigquery
###############################################################################

resource "google_bigquery_dataset" "sap_pmr" {
  dataset_id = "sap_pmr"
  location   = local.bigquery_location
  labels = {
    app = "sappmr-ingestor"
  }
}

resource "google_bigquery_dataset_iam_member" "sap_pmr" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sappmr_ingestor_email}",
      roles  = ["roles/bigquery.dataEditor", "roles/bigquery.user"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)

  dataset_id = google_bigquery_dataset.sap_pmr.dataset_id
  role       = each.value.role
  member     = each.value.member
}

resource "google_bigquery_table" "sap_pmr_offers" {
  dataset_id          = google_bigquery_dataset.sap_pmr.dataset_id
  table_id            = "offers"
  deletion_protection = false
  labels = {
    app = "sappmr-ingestor"
  }
  schema = file("schema/sap_pmr/sap_pmr_offers.json")
  time_partitioning {
    expiration_ms            = 90 * 24 * 60 * 60 * 1000 # 90 days
    field                    = "snapshot_at"
    type                     = "DAY"
    require_partition_filter = true
  }
}

###############################################################################
# OCI
###############################################################################

resource "google_artifact_registry_repository" "sappmr_ingestor" {
  provider      = google-beta
  location      = var.region
  repository_id = "sappmr-ingestor"
  format        = "DOCKER"
}

resource "google_artifact_registry_repository_iam_member" "sappmr_ingestor" {
  provider = google-beta
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sappmr_ingestor_cd_email}",
      roles  = ["roles/artifactregistry.writer"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)

  repository = google_artifact_registry_repository.sappmr_ingestor.name
  location   = var.region

  role   = each.value.role
  member = each.value.member
}

###############################################################################
# cloud run
###############################################################################

resource "google_cloud_run_service" "sappmr_ingestor" {
  name     = "sappmr-ingestor"
  location = var.region

  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/placeholder@sha256:7d54127088b3ddde157225f0047d9ca0cfb56dcf9a923b6f77579236f9a54d2f"

        env {
          name  = "pmr_ingestor_target_uri"
          value = "bigquery://${var.project_id}/${google_bigquery_dataset.sap_pmr.dataset_id}"
        }
        env {
          name  = "PYTHON_LOGGING_LEVEL"
          value = "INFO"
        }
      }
      container_concurrency = 10
      service_account_name  = local.sappmr_ingestor_email
      timeout_seconds       = (9.5) * 60
    }
    metadata {
      # https://cloud.google.com/run/docs/reference/rest/v1/RevisionTemplate
      annotations = {
        "autoscaling.knative.dev/minScale" = 0
        "autoscaling.knative.dev/maxScale" = 1
        "run.googleapis.com/ingress"       = "internal"
      }
    }
  }

  lifecycle {
    ignore_changes = [
      template[0].spec[0].containers[0].image
    ]
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  metadata {
    labels = {
      app = "sappmr-ingestor"
    }
  }
  autogenerate_revision_name = true
}

resource "google_cloud_run_service_iam_member" "sappmr_ingestor" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sappmr_ingestor_email}",
      roles  = ["roles/run.invoker"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)

  service  = google_cloud_run_service.sappmr_ingestor.name
  location = var.region
  role     = each.value.role
  member   = each.value.member
}
