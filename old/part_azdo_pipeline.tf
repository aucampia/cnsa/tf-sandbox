###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "azdo_pipeline" {
  account_id   = local.azdo_pipeline_sa_id
  display_name = local.azdo_pipeline_sa_id
}

resource "google_project_iam_member" "azdo_pipeline" {
  for_each = toset([
    "roles/cloudfunctions.admin",
  ])
  project = data.google_project.project.id
  role    = each.value
  member  = "serviceAccount:${google_service_account.azdo_pipeline.email}"
}


# This file contains things specific to the Product Information Service.

locals {
  azdo_pipeline_sa_id = "azdo-pipeline"
}


data "google_service_account" "azdo_pipeline" {
  account_id = local.azdo_pipeline_sa_id
}
