# Notebook instance for analysis


###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "hello_notebook" {
  account_id   = "hello-notebook"
  display_name = "hello-notebook"
}

###############################################################################
# Network
###############################################################################

module "gcp_network_europewest1b" {
  source  = "terraform-google-modules/network/google"
  version = "~> 4.0"

  project_id   = data.google_project.project.project_id
  network_name = "europewest1b"
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.132.4.0/24"
      subnet_region = "europe-west1"
    },
  ]

  secondary_ranges = {
    subnet-01 = [
      {
        range_name    = "subnet-01-secondary-01"
        ip_cidr_range = "10.120.0.0/24"
      },
    ]
  }
}


###############################################################################
# Project Level
###############################################################################

resource "google_notebooks_instance" "hello" {
  name     = "hello"
  location = "europe-west1-b"
  # location = var.region
  machine_type = "n1-standard-4"
  vm_image {
    project      = "deeplearning-platform-release"
    image_family = "tf2-ent-2-6-cu110-notebooks"
  }

  metadata = {
    proxy-mode = "service_account"
    terraform  = "true"
  }

  service_account = google_service_account.hello_notebook.email

  network = module.gcp_network_europewest1b.network_id
  subnet  = module.gcp_network_europewest1b.subnets_ids[0]
}

resource "google_notebooks_instance_iam_member" "nb_access" {
  for_each = toset([
    "user:iwan.aucamp@coop.no"
  ])

  project       = google_notebooks_instance.hello.project
  location      = google_notebooks_instance.hello.location
  instance_name = google_notebooks_instance.hello.name

  role   = "roles/notebooks.admin"
  member = each.key
}


resource "google_service_account_iam_member" "nb_access" {
  for_each = toset([
    "user:iwan.aucamp@coop.no"
  ])

  service_account_id = google_service_account.hello_notebook.name
  role               = "roles/iam.serviceAccountUser"
  member             = each.key
}
