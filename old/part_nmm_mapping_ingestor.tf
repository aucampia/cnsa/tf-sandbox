###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "nmm_mapping_ingestor" {
  account_id   = local.nmm_mapping_ingestor_sa_id
  display_name = local.nmm_mapping_ingestor_sa_id
}

# This file contains things specific to the Product Information Service.

locals {
  nmm_mapping_ingestor_sa_id = "nmm-mapping-ingestor"
}

###############################################################################
# GCP Service Account
###############################################################################



data "google_service_account" "nmm_mapping_ingestor" {
  account_id = local.nmm_mapping_ingestor_sa_id
}


resource "google_service_account_iam_member" "nmm_mapping_ingestor" {
  service_account_id = data.google_service_account.nmm_mapping_ingestor.name
  for_each = merge([for item in [
    {
      member = "serviceAccount:${data.google_service_account.azdo_pipeline.email}",
      roles  = ["roles/iam.serviceAccountUser"],
    },
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  role   = each.value.role
  member = each.value.member
}

resource "google_bigquery_dataset" "nmm" {
  dataset_id                 = "nmm"
  description                = "Temporary dataset to store the mapping between users' ids before NMM release and after"
  location                   = "EU"
  delete_contents_on_destroy = true
}

resource "google_bigquery_table" "mapping" {
  dataset_id          = google_bigquery_dataset.nmm.dataset_id
  table_id            = "mapping"
  description         = "Temporary table to store the mapping between users' ids before NMM release and after"
  schema              = ""
  deletion_protection = false
  lifecycle {
    ignore_changes = [
      schema,
    ]
  }
}

resource "google_bigquery_dataset_iam_member" "nmm_editors" {
  for_each = toset([
    "serviceAccount:${data.google_service_account.nmm_mapping_ingestor.email}",
  ])
  dataset_id = google_bigquery_dataset.nmm.dataset_id
  role       = "roles/bigquery.dataEditor"
  member     = each.value
}

resource "google_bigquery_table_iam_member" "mapping_editors" {
  for_each = toset([
    "serviceAccount:${data.google_service_account.nmm_mapping_ingestor.email}",
  ])
  dataset_id = google_bigquery_table.mapping.dataset_id
  table_id   = google_bigquery_table.mapping.table_id
  role       = "roles/bigquery.dataEditor"
  member     = each.value
}

#tfsec:ignore:google-storage-enable-ubla
resource "google_storage_bucket" "nmm_mapping_input" {
  name          = "coopnorge-${var.environment}-nmm-mapping-input" #tfsec:ignore:GCP002
  force_destroy = true
  location      = var.region

  lifecycle_rule {
    action {
      type          = "SetStorageClass"
      storage_class = "ARCHIVE"
    }
    condition {
      age = 90
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      age = 120
    }
  }
}



resource "google_storage_bucket_iam_member" "nmm_mapping_input_readers" {
  for_each = toset([
    "serviceAccount:${data.google_service_account.nmm_mapping_ingestor.email}",
  ])
  bucket = google_storage_bucket.nmm_mapping_input.name
  role   = "roles/storage.objectViewer"
  member = each.value
}

# resource "google_storage_bucket_iam_member" "nmm_mapping_input_editor" {
#   for_each = toset([
#     "data-platform",
#   ])
#   bucket = google_storage_bucket.nmm_mapping_input.name
#   role   = "roles/storage.objectAdmin"
#   member = "group:${data.terraform_remote_state.cloud_project_teams.outputs.teams[each.key].gcp.group_key[0].id}"
# }
