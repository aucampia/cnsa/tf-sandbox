
###############################################################################
# cloud build
###############################################################################

# https://cloud.google.com/build/docs/subscribe-build-notifications
resource "google_pubsub_topic" "cloud_builds" {
  name                       = "cloud-builds"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "cloud_builds" {
  name                       = "cloud-builds"
  topic                      = google_pubsub_topic.cloud_builds.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

# resource "google_storage_bucket" "cloud_build_logs" {
#   name     = "coopnorge-${var.project_key}-${var.environment}-cblogs"
#   location = var.region


#   lifecycle_rule {
#     condition {
#       age = 1
#     }
#     action {
#       type = "Delete"
#     }
#   }
# }

# resource "google_storage_bucket_iam_member" "cloud_build_logs" {
#   bucket = google_storage_bucket.cloud_build_logs.name
#   role   = "roles/storage.admin"
#   member = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

resource "google_project_iam_member" "cloud_builds" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudscheduler.iam.gserviceaccount.com"
      roles = [
        "roles/pubsub.publisher",
      ],
    },
    # {
    #   member = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
    #   roles = [
    #     "roles/logging.logWriter",
    #   ]
    # }
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  project = data.google_project.project.id
  role    = each.value.role
  member  = each.value.member
}
