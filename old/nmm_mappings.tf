###############################################################################
# Variables
###############################################################################

locals {
  sapcrm_dump_handler_sa_id    = "sapcrm-dump-handler"
  sapcrm_dump_handler_sa_email = "${local.sapcrm_dump_handler_sa_id}@${var.project_id}.iam.gserviceaccount.com"
  # These are TF module teams that should be able to trigger the handler.
  sapcrm_dump_handler_trigger_teams = ["data-platform", "customer-data"]
}

###############################################################################
# Project Level
###############################################################################

resource "google_service_account" "sapcrm_dump_handler" {
  account_id   = local.sapcrm_dump_handler_sa_id
  display_name = local.sapcrm_dump_handler_sa_id
}

resource "google_project_iam_member" "sapcrm_dump_handler" {
  for_each = merge([for item in [
    {
      member = "serviceAccount:${local.sapcrm_dump_handler_sa_email}"
      roles = [
        "roles/logging.logWriter",
        # "roles/cloudbuild.serviceAgent",
      ],
    }
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  project = data.google_project.project.id
  role    = each.value.role
  member  = each.value.member
  depends_on = [
    google_service_account.sapcrm_dump_handler
  ]
}

###############################################################################
# GCP Service Account
###############################################################################

data "google_service_account" "sapcrm_dump_handler" {
  account_id = local.sapcrm_dump_handler_sa_id

  depends_on = [
    google_service_account.sapcrm_dump_handler
  ]
}

###############################################################################
# Secrets accesss
###############################################################################

resource "google_secret_manager_secret_iam_member" "sapcrm_dump_handler" {
  secret_id = google_secret_manager_secret.sap_sftp_password.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${local.sapcrm_dump_handler_sa_email}"
}

###############################################################################
# Google Cloud Storage
###############################################################################

resource "google_storage_bucket" "nmm_mappings" {
  name                        = "coopnorge-customerdata-${var.environment}-nmm-mappings"
  location                    = local.gcs_location
  uniform_bucket_level_access = true
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]

  lifecycle_rule {
    condition {
      age = 90
    }
    action {
      type          = "SetStorageClass"
      storage_class = "ARCHIVE"
    }
  }
}

#tfsec:ignore:google-iam-no-privileged-service-accounts
resource "google_storage_bucket_iam_member" "nmm_mappings" {
  for_each = { for item in [
    {
      member = "serviceAccount:${local.sapcrm_dump_handler_sa_email}",
      role   = "roles/storage.objectAdmin",
    },
    ] :
    "${item.member}/${item.role}" => item
  }
  bucket = google_storage_bucket.nmm_mappings.name
  role   = each.value.role
  member = each.value.member
}

###############################################################################
# Cloud Build
###############################################################################

resource "google_pubsub_topic" "nmm_mapping_fetcher" {
  name                       = "nmm-mapping-fetcher"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_topic_iam_member" "nmm_mapping_fetcher_teams" {
  for_each = { for value in setproduct(
    local.sapcrm_dump_handler_trigger_teams,
    [
      "roles/pubsub.publisher",
      "roles/pubsub.viewer",
    ]
  ) : "${value[0]}-${value[1]}" => value }
  topic  = google_pubsub_topic.nmm_mapping_fetcher.name
  role   = each.value[1]
  member = "group:${data.terraform_remote_state.cloud_project_teams.outputs.teams[each.value[0]].gcp.group_key[0].id}"
}


resource "google_pubsub_subscription" "nmm_mapping_fetcher" {
  name                       = "nmm-mapping-fetcher"
  topic                      = google_pubsub_topic.nmm_mapping_fetcher.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_cloud_scheduler_job" "nmm_mapping_fetcher" {
  region      = local.cloud_scheduler_region
  name        = "nmm-mapping-fetcher"
  description = "nmm-mapping-fetcher"
  schedule    = "0 0 1 1 0"

  pubsub_target {
    topic_name = google_pubsub_topic.nmm_mapping_fetcher.id
    attributes = {
      action = "run"
    }
  }
}

resource "google_cloudbuild_worker_pool" "pool" {
  name     = "pool"
  location = local.cloudbuild_location
  network_config {
    peered_network = data.google_compute_network.coop.id
  }
}

# https://cloud.google.com/build/docs/build-config-file-schema
# https://cloud.google.com/build/docs/api/reference/rest/v1/projects.builds#Build.LoggingMode
resource "google_cloudbuild_trigger" "nmm_mapping_fetcher" {
  name = "nmm-mapping-fetcher"
  pubsub_config {
    topic                 = google_pubsub_topic.nmm_mapping_fetcher.id
    service_account_email = data.google_service_account.sapcrm_dump_handler.email
  }
  service_account = data.google_service_account.sapcrm_dump_handler.id

  # https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values
  # https://cloud.google.com/build/docs/configuring-builds/use-bash-and-bindings-in-substitutions
  # https://cloud.google.com/build/docs/automating-builds/create-pubsub-triggers
  substitutions = {
    # This is the pubsub message body.
    _PUBSUB_MESSAGE_BODY = "$(body)"
  }

  build {
    step {
      name       = "docker.io/alpine/socat:latest@sha256:acddcaba75c107cf807d1131788e931163d2fa778bc229ff97b32ca85954842c"
      entrypoint = "sh"
      args = ["-c",
        <<-EOT
        set -x
        wget -q -O /dev/stdout https://checkip.amazonaws.com
        for remote in 10.193.113.27 10.193.115.188 172.16.2.3 172.16.2.4
        do
          traceroute -q 2 -w 2 -m 10 "$$${remote}"
          socat -v -ddd OPEN:/dev/null "TCP4:$$${remote}:21,connect-timeout=5"
          busybox ip route get "$$${remote}"
          ping -c 4 "$$${remote}"
        done
        EOT
      ]

    }
    step {
      name       = "docker.io/rclone/rclone:1@sha256:d140bd423eb06f55eb5b4474429e0da01862590fe0e1221460fc275355174720"
      entrypoint = "sh"
      args = ["-euo", "pipefail", "-c",
        <<-EOT
        printenv PUBSUB_MESSAGE_BODY
        printenv SAP_SFTP_PASSWORD | rclone obscure - > /tmp/password.obscure
        RCLONE_CONFIG_SRC_PASS="$(cat /tmp/password.obscure)" || exit
        export RCLONE_CONFIG_SRC_PASS

        rclone ls dst:${google_storage_bucket.nmm_mappings.name}/
        rclone ls src:${local.sap_sftp_path_env_prefix}/
        rclone move src:${local.sap_sftp_path_env_prefix}/ dst:${google_storage_bucket.nmm_mappings.name}/ --dry-run
        EOT
      ]
      env = [
        "PUBSUB_MESSAGE_BODY=$_PUBSUB_MESSAGE_BODY",
        "RCLONE_USE_JSON_LOG=true",
        "RCLONE_LOG_FORMAT=date,time,microseconds,pid,longfile,shortfile,UTC",
        "RCLONE_LOG_LEVEL=DEBUG",
        "RCLONE_STATS=5s",
        "RCLONE_STATS_ONE_LINE_DATE=true",
        "RCLONE_TIMEOUT=1m",
        "RCLONE_CONTIMEOUT=30s",
        "RCLONE_IMMUTABLE=true",
        "RCLONE_CHECKSUM=true",
        "RCLONE_CONFIG_SRC_TYPE=sftp",
        "RCLONE_CONFIG_SRC_USE_FSTAT=true",
        "RCLONE_CONFIG_SRC_DISABLE_CONCURRENT_READS=true",
        "RCLONE_CONFIG_SRC_HOST=${local.sap_sftp_ip}",
        "RCLONE_CONFIG_SRC_PORT=21",
        "RCLONE_CONFIG_SRC_USER=${local.sap_sftp_username}",
        "RCLONE_GCS_BUCKET_POLICY_ONLY=true",
        "RCLONE_CONFIG_DST_TYPE=gcs",
        "RCLONE_CONFIG_DST_GCS_PROJECT_NUMBER=${data.google_project.project.number}",
        "RCLONE_CONFIG_DST_GCS_LOCATION=${local.gcs_location}",
      ]
      secret_env = ["SAP_SFTP_PASSWORD"]
    }
    available_secrets {
      secret_manager {
        env          = "SAP_SFTP_PASSWORD"
        version_name = "${google_secret_manager_secret.sap_sftp_password.id}/versions/latest"
      }
    }
    options {
      logging     = "STACKDRIVER_ONLY"
      worker_pool = google_cloudbuild_worker_pool.pool.id
    }
  }
  # lifecycle {
  #   ignore_changes = [
  #     build.0.step.0.name
  #   ]
  # }
}
