###############################################################################
# bigquery dataset
###############################################################################

resource "google_bigquery_dataset" "laimahvu" {
  dataset_id = "laimahvu"
  location   = local.bigquery_location
}

# resource "google_bigquery_dataset_iam_member" "laimahvu" {
#   for_each = merge([for item in(var.environment != "production" ? [
#     {
#       members = [
#         "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com",
#       ],
#       roles = [
#         "roles/bigquery.dataEditor",
#         "roles/bigquery.metadataViewer",
#       ],
#     },
#     ] : []) : {
#     for tpl in setproduct(item.members, item.roles) :
#     "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
#     }
#   ]...)
#   member     = each.value.member
#   role       = each.value.role
#   dataset_id = google_bigquery_dataset.laimahvu.dataset_id
# }

###############################################################################
# pubsub topic : schemaless
###############################################################################

resource "google_pubsub_topic" "laimahvu_schemaless" {
  name                       = "laimahvu-schemaless"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "laimahvu_schemaless_pull" {
  name                       = "laimahvu-schemaless-pull"
  topic                      = google_pubsub_topic.laimahvu_schemaless.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  retry_policy {
    minimum_backoff = "1s"
    maximum_backoff = "60s"
  }
  dead_letter_policy {
    dead_letter_topic     = google_pubsub_topic.laimahvu_schemaless_pull_dl.id
    max_delivery_attempts = 10
  }
}

resource "google_pubsub_topic" "laimahvu_schemaless_pull_dl" {
  name                       = "laimahvu-schemaless-pull-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "laimahvu_schemaless_pull_dl" {
  name                       = "laimahvu-schemaless-pull-dl"
  topic                      = google_pubsub_topic.laimahvu_schemaless_pull_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_topic" "laimahvu_schemaless_bq_dl" {
  name                       = "laimahvu-schemaless-bq-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "laimahvu_schemaless_bq_dl" {
  name                       = "laimahvu-schemaless-bq-dl"
  topic                      = google_pubsub_topic.laimahvu_schemaless_bq_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}


###############################################################################
# bigquery table : schemaless
###############################################################################

resource "google_bigquery_table" "laimahvu_schemaless" {
  dataset_id = google_bigquery_dataset.laimahvu.dataset_id
  table_id   = "laimahvu-schemaless"

  time_partitioning {
    field = "publish_time"
    type  = "DAY"
    # require_partition_filter = true
  }

  deletion_protection = false

  schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
}

###############################################################################
# pubsub topic : cloudevent
###############################################################################

resource "google_pubsub_schema" "laimahvu_cloudevent" {
  name       = "laimahvu-cloudevent"
  type       = "PROTOCOL_BUFFER"
  definition = file("spec/proto/cloudevents_standalone_pubsub.proto")
}

resource "google_pubsub_topic" "laimahvu_cloudevent" {
  name                       = "laimahvu-cloudevent"
  message_retention_duration = "${7 * 24 * 60 * 60}s"

  schema_settings {
    schema   = google_pubsub_schema.laimahvu_cloudevent.id
    encoding = "BINARY"
  }
}

resource "google_pubsub_subscription" "laimahvu_cloudevent_pull" {
  name                       = "laimahvu-cloudevent-pull"
  topic                      = google_pubsub_topic.laimahvu_cloudevent.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  retry_policy {
    minimum_backoff = "1s"
    maximum_backoff = "60s"
  }
  dead_letter_policy {
    dead_letter_topic     = google_pubsub_topic.laimahvu_cloudevent_pull_dl.id
    max_delivery_attempts = 10
  }
}

resource "google_pubsub_topic" "laimahvu_cloudevent_pull_dl" {
  name                       = "laimahvu-cloudevent-pull-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

# resource "google_pubsub_topic_iam_member" "laimahvu_cloudevent_pull_dl" {
#   for_each = merge([for item in(var.environment != "production" ? [
#     {
#       members = [
#         "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com",
#       ],
#       roles = [
#         "roles/pubsub.publisher",
#       ],
#     },
#     ] : []) : {
#     for tpl in setproduct(item.members, item.roles) :
#     "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
#     }
#   ]...)
#   member = each.value.member
#   role   = each.value.role
#   topic  = google_pubsub_topic.laimahvu_cloudevent_pull_dl.name
# }

resource "google_pubsub_subscription" "laimahvu_cloudevent_pull_dl" {
  name                       = "laimahvu-cloudevent-pull-dl"
  topic                      = google_pubsub_topic.laimahvu_cloudevent_pull_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_topic" "laimahvu_cloudevent_bq_dl" {
  name                       = "laimahvu-cloudevent-bq-dl"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

# resource "google_pubsub_topic_iam_member" "laimahvu_cloudevent_bq_dl" {
#   for_each = merge([for item in(var.environment != "production" ? [
#     {
#       members = [
#         "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com",
#       ],
#       roles = [
#         "roles/pubsub.publisher",
#       ],
#     },
#     ] : []) : {
#     for tpl in setproduct(item.members, item.roles) :
#     "${tpl[0]}-${tpl[1]}}" => { member = tpl[0], role = tpl[1] }
#     }
#   ]...)
#   member = each.value.member
#   role   = each.value.role
#   topic  = google_pubsub_topic.laimahvu_cloudevent_bq_dl.name
# }

resource "google_pubsub_subscription" "laimahvu_cloudevent_bq_dl" {
  name                       = "laimahvu-cloudevent-bq-dl"
  topic                      = google_pubsub_topic.laimahvu_cloudevent_bq_dl.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

###############################################################################
# bigquery table : cloudevent
###############################################################################

resource "google_bigquery_table" "laimahvu_cloudevent" {
  dataset_id = google_bigquery_dataset.laimahvu.dataset_id
  table_id   = "laimahvu-cloudevent"

  time_partitioning {
    field = "publish_time"
    type  = "DAY"
    # require_partition_filter = true
  }

  deletion_protection = false

  schema = file("spec/bigquery/pubsub_bigquery_cloudevents.json")
  # schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
}

