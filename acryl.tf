# ###############################################################################
# # variables
# ###############################################################################

# locals {
#   acryl_main_sa_id    = "acryl-main"
#   acryl_main_sa_email = "${local.acryl_main_sa_id}@${var.project_id}.iam.gserviceaccount.com"
# }

# resource "google_service_account" "acryl_main" {
#   account_id = local.acryl_main_sa_id
# }


# resource "google_service_account_key" "acryl_main" {
#   service_account_id = google_service_account.acryl_main.id
# }

# resource "google_secret_manager_secret" "acryl_main_sak" {
#   secret_id = "acryl-main-sak"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

# resource "google_project_iam_custom_role" "acryl_ingestor" {
#   role_id = "acryl.ingestor"
#   title   = "Acryl Ingestor"
#   permissions = [
#     "bigquery.readsessions.create",
#     "bigquery.datasets.get",
#     "bigquery.datasets.getIamPolicy",
#     "bigquery.models.getMetadata",
#     "bigquery.models.list",
#     "bigquery.routines.get",
#     "bigquery.routines.list",
#     "bigquery.tables.get",
#     "bigquery.tables.getIamPolicy",
#     "bigquery.tables.list",
#     "bigquery.config.get",
#     "bigquery.jobs.create",
#     # "resourcemanager.projects.get",
#     # "resourcemanager.projects.list",
#   ]
# }

# resource "google_secret_manager_secret_version" "acryl_main_sak" {
#   secret      = google_secret_manager_secret.acryl_main_sak.id
#   secret_data = base64decode(google_service_account_key.acryl_main.private_key)
# }


# resource "google_project_iam_member" "acryl_main" {
#   for_each = toset([
#     # "roles/logging.viewAccessor",
#     # "roles/bigquery.metadataViewer",
#     # "roles/bigquery.jobUser",
#     "projects/${var.project_id}/roles/acryl.ingestor",
#   ])
#   project = var.project_id
#   role    = each.value
#   member  = "serviceAccount:${google_service_account.acryl_main.email}"
# }


# ###############################################################################
# #
# ###############################################################################

# # gcloud secrets versions access latest --project="sandbox-iwan-aucamp-e332" --secret="acryl-main-sak"

# resource "google_bigquery_table" "coxaitho_000" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-000"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }

# resource "google_bigquery_table" "coxaitho_001" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-001"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }

# resource "google_bigquery_table" "coxaitho_002" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-002"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }

# resource "google_secret_manager_secret" "acryl_admin_password" {
#   secret_id = "acryl-admin-password"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
# }

resource "google_secret_manager_secret" "acryl_access_token" {
  secret_id = "acryl-access-token"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

# resource "google_secret_manager_secret_iam_member" "acryl" {
#   for_each = merge([for item in([
#     {
#       members = [
#         "group:${data.terraform_remote_state.cloud_project_teams.outputs.teams["data-platform"].gcp.group_key[0].id}",
#       ],
#       roles = [
#         "roles/secretmanager.secretVersionAdder",
#         "roles/secretmanager.viewer",
#       ],
#       secret_ids = [
#         "projects/${var.project_id}/secrets/acryl-admin-password",
#         "projects/${var.project_id}/secrets/acryl-access-token",
#       ],
#     },
#     ]) : {
#     for tpl in setproduct(item.members, item.roles, item.secret_ids) :
#     "${tpl[0]}-${tpl[1]}-${tpl[2]}" => { member = tpl[0], role = tpl[1], secret_id = tpl[2], }
#     }
#   ]...)
#   member    = each.value.member
#   role      = each.value.role
#   secret_id = each.value.secret_id
#   depends_on = [
#     google_secret_manager_secret.acryl_admin_password,
#     google_secret_manager_secret.acryl_access_token,
#   ]
# }
