data "terraform_remote_state" "cloud_project_teams" {
  backend = "remote"

  config = {
    hostname     = "terraform.coop.no"
    organization = "coopnorge"
    workspaces = {
      name = "cloud-projects-teams"
    }
  }
}
