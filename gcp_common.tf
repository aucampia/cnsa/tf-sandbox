resource "google_service_account" "common" {
  account_id   = "common"
  display_name = "common service account"
}

// https://github.com/hashicorp/terraform-provider-google/blob/8a3a0b243cc07bc0f2e1820acacbd3aeafb38dcb/google/resource_google_service_account_key.go
resource "google_service_account_key" "common_rsa2048_x509pem_gcf" {
  service_account_id = google_service_account.common.id
  key_algorithm      = "KEY_ALG_RSA_2048"
  public_key_type    = "TYPE_X509_PEM_FILE"
  private_key_type   = "TYPE_GOOGLE_CREDENTIALS_FILE"
}

resource "google_service_account_key" "common_rsa2048_x509pem_p12" {
  service_account_id = google_service_account.common.id
  key_algorithm      = "KEY_ALG_RSA_2048"
  public_key_type    = "TYPE_X509_PEM_FILE"
  private_key_type   = "TYPE_PKCS12_FILE"
}

resource "google_artifact_registry_repository" "common_docker" {
  provider      = google-beta
  location      = local.artifact_registry_location
  repository_id = "common-docker"
  format        = "DOCKER"
}


resource "google_artifact_registry_repository_iam_member" "common_docker" {
  provider = google-beta
  for_each = merge([for item in concat([
    {
      member = "serviceAccount:common@${var.project_id}.iam.gserviceaccount.com",
      roles  = ["roles/artifactregistry.writer"],
    },
    {
      member = "user:iwan.aucamp@coop.no",
      roles  = ["roles/artifactregistry.writer"],
    },
    {
      member = "domain:coop.no",
      roles  = ["roles/artifactregistry.reader"],
    },
    {
      member = "serviceAccount:common@${var.project_id}.iam.gserviceaccount.com",
      roles  = ["roles/artifactregistry.reader"],
    },
    ],
    [

    ],
    ) : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)

  repository = google_artifact_registry_repository.common_docker.name
  location   = google_artifact_registry_repository.common_docker.location

  role   = each.value.role
  member = each.value.member
}
