# ###############################################################################
# # variables
# ###############################################################################

# locals {
#   acryl_main_sa_id    = "acryl-main"
#   acryl_main_sa_email = "${local.acryl_main_sa_id}@${var.project_id}.iam.gserviceaccount.com"
# }

# resource "google_service_account" "acryl_main" {
#   account_id = local.acryl_main_sa_id
# }


resource "google_service_account_key" "acryl_ingestor" {
  service_account_id = google_service_account.acryl_ingestor.id
}

resource "google_secret_manager_secret" "acryl_ingestor_sak" {
  secret_id = "acryl-ingestor-sak"
  replication {
    user_managed {
      replicas {
        location = "europe-north1"
      }
      replicas {
        location = "europe-west6"
      }
    }
  }
}

resource "google_project_iam_custom_role" "acryl_ingestor" {
  role_id     = "datahub.bigQueryMetadataIngestor"
  title       = "DataHub BigQuery Metadata Ingestor"
  description = "This role grants permissions that Acryl / DataHub needs to ingest metadata from BigQuery, it does not include any permissions to reading the actual data from BigQuery tables."
  # This role exists because DataHub needs the bigquery.readsessions.create
  # permission but the only standard roles that include this permission also
  # includes a bunch of other permissions which are not desirable.
  permissions = [
    "bigquery.readsessions.create",
    "bigquery.readsessions.getData",
    "bigquery.datasets.get",
    "bigquery.datasets.getIamPolicy",
    "bigquery.models.getMetadata",
    "bigquery.models.list",
    "bigquery.routines.get",
    "bigquery.routines.list",
    "bigquery.tables.get",
    "bigquery.tables.getIamPolicy",
    "bigquery.tables.list",
    "bigquery.config.get",
    "bigquery.jobs.create",
  ]
}

resource "google_secret_manager_secret_version" "acryl_ingestor_sak" {
  secret      = google_secret_manager_secret.acryl_ingestor_sak.id
  secret_data = base64decode(google_service_account_key.acryl_ingestor.private_key)
}


# resource "google_project_iam_member" "acryl_main" {
#   for_each = toset([
#     # "roles/logging.viewAccessor",
#     # "roles/bigquery.metadataViewer",
#     # "roles/bigquery.jobUser",
#     "projects/${var.project_id}/roles/acryl.ingestor",
#   ])
#   project = var.project_id
#   role    = each.value
#   member  = "serviceAccount:${google_service_account.acryl_main.email}"
# }


# ###############################################################################
# #
# ###############################################################################

# # gcloud secrets versions access latest --project="sandbox-iwan-aucamp-e332" --secret="acryl-ingestor-sak"

# resource "google_bigquery_table" "coxaitho_000" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-000"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }

# resource "google_bigquery_table" "coxaitho_001" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-001"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }

# resource "google_bigquery_table" "coxaitho_002" {
#   dataset_id = google_bigquery_dataset.scratchpad.dataset_id
#   table_id   = "coxaitho-002"

#   deletion_protection = false

#   schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
# }
