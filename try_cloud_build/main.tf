###############################################################################
# common
###############################################################################

data "google_project" "project" {
}

###############################################################################
# service-account
###############################################################################

resource "google_service_account" "cb_ohmietoh" {
  account_id   = "cb-ohmietoh"
  display_name = "SAP PMR Ingestor"
}

# https://cloud.google.com/iam/docs/understanding-roles#cloud-build-roles
# https://cloud.google.com/build/docs/iam-roles-permissions#predefined_roles
# https://cloud.google.com/build/docs/cloud-build-service-account#default_permissions_of_service_account
# https://cloud.google.com/build/docs/securing-builds/configure-user-specified-service-accounts#before_you_begin
resource "google_project_iam_member" "cb_ohmietoh" {
  for_each = toset([
    "roles/logging.logWriter",
    # "roles/logging.bucketWriter",
    # "roles/storage.objectAdmin",
    # "roles/cloudbuild.builds.builder",
    # "roles/storage.admin",
  ])
  project = data.google_project.project.project_id
  role    = each.value
  member  = "serviceAccount:${google_service_account.cb_ohmietoh.email}"
}

# # https://cloud.google.com/iam/docs/understanding-roles#logging-roles
# resource "google_project_iam_member" "cb_ohmietoh" {
#   project = data.google_project.project.project_id
#   role    = "roles/cloudbuild.builds.builder"
#   member  = "serviceAccount:${google_service_account.cb_ohmietoh.email}"
# }

###############################################################################
# secrets
###############################################################################



###############################################################################
# log bucket
###############################################################################

resource "google_storage_bucket" "cb_ohmietoh_logs" {
  name     = "cnsa-iwana-dev-cb-ohmietoh-logs"
  location = "europe-north1"
}

# https://cloud.google.com/logging/docs/buckets
resource "google_storage_bucket_iam_member" "cb_ohmietoh_logs" {
  # "roles/storage.objectAdmin",
  # "roles/logging.bucketWriter",
  for_each = { for item in [
    {
      member = "serviceAccount:${google_service_account.cb_ohmietoh.email}",
      role   = "roles/storage.admin",
    },
    ] :
    "${item.member}/${item.role}" => item
  }
  bucket = google_storage_bucket.cb_ohmietoh_logs.name
  role   = each.value.role
  member = each.value.member
}



# https://cloud.google.com/build/docs/securing-builds/store-manage-build-logs#store-custom-bucket
# https://cloud.google.com/build/docs/cloud-build-service-account

# resource "google_storage_bucket_iam_member" "cb_ohmietoh_logs_cb" {
#   bucket = google_storage_bucket.cb_ohmietoh_logs.name
#   role   = "roles/storage.objectAdmin"
#   member = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
# }

# resource "google_storage_bucket_iam_member" "cb_ohmietoh_logs" {
#   bucket = google_storage_bucket.cb_ohmietoh_logs.name
#   role   = "roles/storage.objectAdmin"
#   member = "serviceAccount:${google_service_account.cb_ohmietoh.email}"
# }

# resource "google_storage_bucket_iam_member" "cb_ohmietoh_logs_sa" {
#   bucket = google_storage_bucket.cb_ohmietoh_logs.name
#   role   = "roles/storage.objectAdmin"
#   member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudbuild.iam.gserviceaccount.com"
# }

###############################################################################
# repos
###############################################################################

# resource "google_sourcerepo_repository" "blank" {
#   name = "blank"
# }

###############################################################################
# pipeline storage
###############################################################################

# resource "google_storage_bucket" "cb_ohmietoh_pipelines" {
#   name     = "cnsa-iwana-dev-cb-ohmietoh-pipelines"
#   location = "europe-north1"
# }

# # https://cloud.google.com/logging/docs/buckets
# resource "google_storage_bucket_iam_member" "cb_ohmietoh_pipelines" {
#   for_each = { for item in [
#     {
#       member = "serviceAccount:${google_service_account.cb_ohmietoh.email}",
#       role   = "roles/storage.objectViewer",
#     },
#     ] :
#     "${item.member}/${item.role}" => item
#   }
#   bucket = google_storage_bucket.cb_ohmietoh_pipelines.name
#   role   = each.value.role
#   member = each.value.member
# }

# # https://cloud.google.com/build/docs/build-config-file-schema
# resource "google_storage_bucket_object" "cb_ohmietoh_pipeline" {
#   bucket = google_storage_bucket.cb_ohmietoh_pipelines.name
#   name = "cloudbuild.yaml"
#   content = yamlencode({
#     steps = [
#       {
#       name       = "gcr.io/cloud-builders/gcloud"
#       entrypoint = "bash"
#       args = ["-eEuo", "pipefail", "-c",
#         <<-EOT
#         gcloud auth list
#         declare -p
#         EOT
#       ]
#       timeout = "120s"
#       }
#     ]
#     logs_bucket = google_storage_bucket.cb_ohmietoh_logs.name
#     # options {
#     #   logging = "CLOUD_LOGGING_ONLY"
#     # }
#   })
# }

###############################################################################
# webhook trigger
###############################################################################

# resource "random_password" "cb_ohmietoh_webhook" {
#   length = 64
# }

# resource "google_secret_manager_secret" "cb_ohmietoh_webhook" {
#   secret_id = "cb_ohmietoh_webhook"
#   replication {
#     user_managed {
#       replicas {
#         location = "europe-north1"
#       }
#       replicas {
#         location = "europe-west6"
#       }
#     }
#   }
#   labels = {
#     app = "cb-ohmietoh"
#   }
# }

# resource "google_secret_manager_secret_version" "cb_ohmietoh_webhook" {
#   secret      = google_secret_manager_secret.cb_ohmietoh_webhook.id
#   secret_data = random_password.cb_ohmietoh_webhook.result
# }

# resource "google_secret_manager_secret_iam_member" "sa_cloudbuild_cb_ohmietoh_webhook" {
#   secret_id = google_secret_manager_secret.cb_ohmietoh_webhook.secret_id
#   role      = "roles/secretmanager.secretAccessor"
#   member    = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudbuild.iam.gserviceaccount.com"
# }

# https://cloud.google.com/build/docs/build-config-file-schema
# https://cloud.google.com/build/docs/api/reference/rest/v1/projects.builds#Build.LoggingMode

# resource "google_cloudbuild_trigger" "ohmietoh_wh" {
#   name = "ohmietoh-wh"

#   webhook_config {
#     secret = google_secret_manager_secret_version.cb_ohmietoh_webhook.id
#   }

#   depends_on = [
#     google_secret_manager_secret_iam_member.sa_cloudbuild_cb_ohmietoh_webhook
#   ]
#   service_account = google_service_account.cb_ohmietoh.id


#   build {
#     step {
#       name       = "gcr.io/cloud-builders/gcloud"
#       entrypoint = "bash"
#       args = ["-eEuo", "pipefail", "-c",
#         <<-EOT
#         gcloud auth list
#         declare -p
#         EOT
#       ]
#       timeout = "120s"
#     }
#     logs_bucket = google_storage_bucket.cb_ohmietoh_logs.name
#     # options {
#     #   logging = "CLOUD_LOGGING_ONLY"
#     # }
#   }
# }

###############################################################################
# pubsub trigger
###############################################################################

/*
gcloud pubsub topics publish \
  --project sandbox-iwana-sandbox-9a54 \
  --attribute=what=test \
  cb-ohmietoh
*/


resource "google_pubsub_topic" "cb_ohmietoh" {
  name                       = "cb-ohmietoh"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_subscription" "cb_ohmietoh" {
  name                       = "cb-ohmietoh"
  topic                      = google_pubsub_topic.cb_ohmietoh.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
  labels = {
    app = "cb-ohmietoh"
  }
}

# resource "google_pubsub_subscription_iam_member" "cb_ohmietoh" {
#   subscription = google_pubsub_subscription.cb_ohmietoh.id
#   role         = "roles/pubsub.editor"
#   member       = "serviceAccount:${google_service_account.cb_ohmietoh.email}"
# }


# https://cloud.google.com/build/docs/build-config-file-schema
resource "google_cloudbuild_trigger" "ohmietoh_ps" {
  name = "ohmietoh-ps"

  pubsub_config {
    topic                 = google_pubsub_topic.cb_ohmietoh.id
    service_account_email = google_service_account.cb_ohmietoh.email
  }
  service_account = google_service_account.cb_ohmietoh.id

  # filename = "cloudbuild.yaml"

  # build =

  # https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values
  # https://cloud.google.com/build/docs/configuring-builds/use-bash-and-bindings-in-substitutions
  substitutions = {
    _BODY = "$(body)"
  }

  build {
    step {
      name       = "gcr.io/cloud-builders/gcloud"
      entrypoint = "bash"
      args = ["-eEuo", "pipefail", "-c",
        <<-EOT
        gcloud auth list
        declare -p
        printenv S_BODY
        EOT
      ]
      timeout = "120s"
      env = [
        "S_BODY=$_BODY"
      ]
    }
    logs_bucket = google_storage_bucket.cb_ohmietoh_logs.name
    # options {
    #   logging = "CLOUD_LOGGING_ONLY"
    # }
  }
}

