

resource "google_project_service" "project_service" {
  for_each = toset([
    "cloudkms.googleapis.com",
    "run.googleapis.com",
    "artifactregistry.googleapis.com",
    "cloudbuild.googleapis.com", # needed for cloud build
    # "appengineflex.googleapis.com",
    "secretmanager.googleapis.com",
    "cloudscheduler.googleapis.com", # needed for cloud build
    # "sourcerepo.googleapis.com",
    "iamcredentials.googleapis.com",
    "iam.googleapis.com", # needed for cloud build
    # "dataflow.googleapis.com",
    # "dataproc.googleapis.com",
    "bigquery.googleapis.com",
    # "bigquerystorage.googleapis.com",
    # "cloudtrace.googleapis.com",
    # "datapipelines.googleapis.com",
    # "firestore.googleapis.com",
    # "run.googleapis.com",
    # "eventarc.googleapis.com",
    # "iamcredentials.googleapis.com",
    # "cloudscheduler.googleapis.com",
    "notebooks.googleapis.com",
    "servicenetworking.googleapis.com",
    "cloudfunctions.googleapis.com",
    "batch.googleapis.com",
  ])
  service                    = each.value
  disable_dependent_services = true
}

data "google_project" "project" {
}

# data "google_service_account" "dataflow" {
#   account_id = "service-${data.google_project.project.number}@dataflow-service-producer-prod.iam.gserviceaccount.com"
# }

# data "google_service_account" "cloudscheduler_sa" {
#   account_id = "service-${data.google_project.project.number}@gcp-sa-cloudscheduler.iam.gserviceaccount.com"
# }


###############################################################################
# KMS setup
###############################################################################
# NOTE: KMS setup to comply with tfsec linting. This adds an additional layer
# of encryption on what would otherwise happen without it.

resource "google_kms_key_ring" "gcs" {
  name     = "gcsb"
  location = "europe-north1"
}

resource "google_kms_crypto_key" "gcs" {
  name            = "gcsb"
  key_ring        = google_kms_key_ring.gcs.id
  rotation_period = "86401s"
}

data "google_storage_project_service_account" "main" {
}

resource "google_kms_crypto_key_iam_binding" "gcs" {
  crypto_key_id = google_kms_crypto_key.gcs.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${data.google_storage_project_service_account.main.email_address}",
  ]
  depends_on = [
    data.google_storage_project_service_account.main,
  ]
}

