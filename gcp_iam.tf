# resource "google_service_account" "pim_syncer" {
#   account_id   = "pim-syncer"
#   display_name = "pim-syncer"
# }

# resource "google_project_iam_member" "sas" {
#   for_each = merge([for item in [
#     {
#       member = "serviceAccount:${google_service_account.pim_syncer.email}"
#       roles = [
#         "roles/logging.logWriter",
#       ],
#     }
#     ] : {
#     for role in item.roles :
#     "${item.member}/${role}" => { member = item.member, role = role }
#     }
#   ]...)
#   project = data.google_project.project.id
#   role    = each.value.role
#   member  = each.value.member
# }


data "google_storage_transfer_project_service_account" "default" {
  project = var.project_id
}

#tfsec:ignore:google-iam-no-user-granted-permissions
#tfsec:ignore:google-iam-no-project-level-service-account-impersonation
resource "google_project_iam_member" "main" {
  for_each = merge([for item in [
    {
      member = "user:iwan.aucamp@coop.no",
      roles = [
        "roles/iam.serviceAccountTokenCreator",
        "roles/iam.serviceAccountUser",
        # "roles/cloudscheduler.admin",
        "roles/serviceusage.serviceUsageConsumer",
      ],
    },
    # {
    #   member = "serviceAccount:terraform@${var.project_id}.iam.gserviceaccount.com"
    #   roles = [
    #     "roles/cloudscheduler.admin",
    #   ],
    # },
    # {
    #   member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-cloudscheduler.iam.gserviceaccount.com"
    #   roles = [
    #     # "roles/cloudscheduler.serviceAgent",
    #     "roles/pubsub.publisher",
    #   ],
    # },
    # {
    #   member = "serviceAccount:${data.google_project.project.number}@cloudbuild.gserviceaccount.com"
    #   roles = [
    #     "roles/logging.logWriter",
    #   ]
    # }
    ] : {
    for role in item.roles :
    "${item.member}/${role}" => { member = item.member, role = role }
    }
  ]...)
  project = data.google_project.project.id
  role    = each.value.role
  member  = each.value.member
}
