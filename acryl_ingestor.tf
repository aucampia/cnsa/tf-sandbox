###############################################################################
# Service Account
###############################################################################

locals {
  acryl_ingestor_sa_id    = "acryl-ingestor"
  acryl_ingestor_sa_email = "${local.acryl_ingestor_sa_id}@${var.project_id}.iam.gserviceaccount.com"
}

resource "google_service_account" "acryl_ingestor" {
  account_id = local.acryl_ingestor_sa_id
}

resource "google_secret_manager_secret_iam_member" "acryl_ingestor" {
  secret_id = google_secret_manager_secret.acryl_access_token.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.acryl_ingestor.email}"
}

resource "google_project_iam_member" "acryl_ingestor" {
  for_each = toset([
    "roles/logging.logWriter",
  ])
  project = var.project_id
  member  = "serviceAccount:${google_service_account.acryl_ingestor.email}"
  role    = each.value
}

###############################################################################
# Google Cloud Storage
###############################################################################

resource "google_storage_bucket" "recipes" {
  name                        = "coopnorge-acryl-${var.environment}-recipes"
  location                    = local.gcs_location
  uniform_bucket_level_access = true
  encryption {
    default_kms_key_name = google_kms_crypto_key.gcs.id
  }

  depends_on = [
    google_kms_crypto_key_iam_binding.gcs
  ]
}

resource "google_storage_bucket_iam_member" "recipes" {
  for_each = { for item in [
    {
      member = "serviceAccount:${local.acryl_ingestor_sa_email}",
      role   = "roles/storage.objectViewer",
    }
    ] :
    "${item.member}/${item.role}" => item
  }
  bucket = google_storage_bucket.recipes.name
  role   = each.value.role
  member = each.value.member
}

data "archive_file" "recipes_archive" {
  type        = "zip"
  source_dir  = "${path.module}/datahub-recipes/"
  output_path = "${path.module}/datahub-recipes.zip"
}


resource "google_storage_bucket_object" "recipes_archive" {
  bucket = google_storage_bucket.recipes.name
  name   = "datahub-recipes.zip"
  source = "${path.module}/datahub-recipes.zip"
}

###############################################################################
# Cloud Build
###############################################################################

resource "google_pubsub_topic" "acryl_ingestor" {
  name                       = "acryl-ingestor"
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_pubsub_topic_iam_member" "acryl_ingestor_invoker_teams" {
  for_each = { for value in setproduct(
    local.invoker_teams,
    [
      "roles/pubsub.publisher",
      "roles/pubsub.viewer",
    ]
  ) : "${value[0]}-${value[1]}" => value }
  topic  = google_pubsub_topic.acryl_ingestor.name
  role   = each.value[1]
  member = "group:${data.terraform_remote_state.cloud_project_teams.outputs.teams[each.value[0]].gcp.group_key[0].id}"
}


resource "google_pubsub_subscription" "acryl_ingestor" {
  name                       = "acryl-ingestor"
  topic                      = google_pubsub_topic.acryl_ingestor.id
  ack_deadline_seconds       = 600
  message_retention_duration = "${7 * 24 * 60 * 60}s"
}

resource "google_cloud_scheduler_job" "acryl_ingestor" {
  region      = local.cloud_scheduler_region
  name        = "acryl-ingestor"
  description = "acryl-ingestor"
  schedule    = "0 4 * * *"

  pubsub_target {
    topic_name = google_pubsub_topic.acryl_ingestor.id
    attributes = {
      action = "run"
    }
  }
}

# https://cloud.google.com/build/docs/build-config-file-schema
# https://cloud.google.com/build/docs/api/reference/rest/v1/projects.builds#Build.LoggingMode
resource "google_cloudbuild_trigger" "acryl_ingestor" {
  name = "acryl-ingestor"
  pubsub_config {
    topic = google_pubsub_topic.acryl_ingestor.id
    # service_account_email = google_service_account.acryl_ingestor.email
  }
  service_account = google_service_account.acryl_ingestor.id

  # https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values
  # https://cloud.google.com/build/docs/configuring-builds/use-bash-and-bindings-in-substitutions
  # https://cloud.google.com/build/docs/automating-builds/create-pubsub-triggers
  # https://cloud.google.com/build/docs/cloud-builders
  # https://github.com/GoogleCloudPlatform/cloud-builders
  # https://console.cloud.google.com/gcr/images/google-containers/GLOBAL
  substitutions = {
    # This is the pubsub message body.
    _PUBSUB_MESSAGE_BODY = "$(body)"
  }

  build {
    step {
      # https://cloud.google.com/build/docs/cloud-builders
      name       = "gcr.io/google.com/cloudsdktool/cloud-sdk:alpine@sha256:bedc795d0f14a6cc9534092b5309dde774f574a3577b3dfbdf22ac6f044165cc"
      entrypoint = "sh"
      args = ["-euo", "pipefail", "-c",
        <<-EOT
        set -x
        gsutil cp "gs://${google_storage_bucket_object.recipes_archive.bucket}/${google_storage_bucket_object.recipes_archive.name}" "/srv/workspace/datahub-recipes.zip"
        unzip -l /srv/workspace/datahub-recipes.zip
        unzip -d /srv/workspace/datahub-recipes /srv/workspace/datahub-recipes.zip
        find /srv/workspace
        EOT
      ]
      env = [
        "PUBSUB_MESSAGE_BODY=$_PUBSUB_MESSAGE_BODY",
      ]
      volumes {
        name = "workspace"
        path = "/srv/workspace"
      }
    }
    step {
      name = "gcr.io/cloud-builders/docker:latest@sha256:ee58706f3c8ce2b4058261f0799018de38fac7601c1e3fd5250f8e5dc562f827"
      args = [
        "run", "--network=cloudbuild", "--rm", "-i",
        "--volume=workspace:/srv/workspace",
        "--volume=home-volume:/root",
        "--entrypoint=",
        "--env=DATAHUB_GMS_URL",
        "--env=DATAHUB_GMS_TOKEN",
        "--user=root:root",
        "docker.io/acryldata/datahub-ingestion:v0.8.43.5@sha256:5fb3139a07e29e7815e87c701a71142530e3a835e22328c92318e3e2824ceb74",
        "bash", "-euo", "pipefail", "-c",
        <<-EOT
        printenv DATAHUB_GMS_TOKEN | wc -l
        printenv DATAHUB_GMS_URL
        find /srv/workspace/datahub-recipes
        datahub --version
        find /srv/workspace/datahub-recipes -maxdepth 1 \( -name '*.yaml' -o -name '*.yml' \) -type f -print0 | xargs -0 -t -I{} datahub ingest run -c {}
        EOT
      ]
      env = [
        "PUBSUB_MESSAGE_BODY=$_PUBSUB_MESSAGE_BODY",
        "DATAHUB_GMS_URL=https://coopnorge.acryl.io/gms",
      ]
      secret_env = ["DATAHUB_GMS_TOKEN"]
      volumes {
        name = "workspace"
        path = "/srv/workspace"
      }
    }
    available_secrets {
      secret_manager {
        env          = "DATAHUB_GMS_TOKEN"
        version_name = "${google_secret_manager_secret.acryl_access_token.id}/versions/latest"
      }
    }
    options {
      logging     = "STACKDRIVER_ONLY"
      worker_pool = google_cloudbuild_worker_pool.default.id
    }
  }
}
