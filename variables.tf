# https://www.terraform.io/docs/language/values/variables.html

# variable "credentials" {
#   type        = string
#   description = "private key for the tf service account"
# }

variable "project_key" {
  type = string
}

variable "project_id" {
  type        = string
  description = "Project ID"
}

variable "region" {
  type        = string
  description = "default region for resources"
}

variable "zone" {
  type        = string
  description = "default zone for resources"
}


variable "environment" {
  type = string
}


variable "subscription_id" {
  type = string
}
variable "tenant_id" {
  type = string
}


# Azure


variable "client_id" {
  type = string
}
variable "client_secret" {
  type = string
}


variable "rg_name" {
  type        = string
  description = "name of the project resourcegroup"
}
variable "kv_name" {
  type        = string
  description = "name of the project keyvault"
}

variable "location" {
  type = string
}
