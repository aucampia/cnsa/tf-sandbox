provider "null" {
}

provider "random" {
}

provider "google-beta" {
  # credentials = var.credentials
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

provider "google" {
  # credentials = var.credentials
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

# provider "azurerm" {
#   features {}
#   subscription_id = var.subscription_id
#   # client_id       = var.client_id
#   # client_secret   = var.client_secret
#   tenant_id = var.tenant_id
# }

# provider "azurerm" {
#   alias = "sp"
#   features {}
#   subscription_id = var.subscription_id
#   client_id       = var.client_id
#   client_secret   = var.client_secret
#   tenant_id       = var.tenant_id
# }

# provider "azuread" {
#   client_id     = var.client_id
#   client_secret = var.client_secret
#   tenant_id     = var.tenant_id
# }

# provider "azuread2" {
#   # client_id     = var.client_id
#   # client_secret = var.client_secret
#   tenant_id     = var.tenant_id
# }

provider "github" {
  # Configuration options
  owner = "iasandbox"
}
