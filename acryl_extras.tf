###############################################################################
# Role
###############################################################################

# resource "google_project_iam_custom_role" "datahub_bigquery_metadata_ingestor" {
#   role_id     = "datahub.bigQueryMetadataIngestor"
#   title       = "DataHub BigQuery Metadata Ingestor"
#   description = "This role grants permissions that Acryl / DataHub needs to ingest metadata from BigQuery, it does not include any permissions to reading the actual data from BigQuery tables."
#   # This role exists because DataHub needs the bigquery.readsessions.create
#   # permission but the only standard roles that include this permission also
#   # includes a bunch of other permissions which are not desirable.
#   permissions = [
#     "bigquery.readsessions.create",
#     "bigquery.datasets.get",
#     "bigquery.datasets.getIamPolicy",
#     "bigquery.models.getMetadata",
#     "bigquery.models.list",
#     "bigquery.routines.get",
#     "bigquery.routines.list",
#     "bigquery.tables.get",
#     "bigquery.tables.getIamPolicy",
#     "bigquery.tables.list",
#     "bigquery.config.get",
#     "bigquery.jobs.create",
#   ]
# }



resource "google_project_iam_member" "acryl_main" {
  for_each = toset([
    # "roles/logging.viewAccessor",
    # "roles/bigquery.metadataViewer",
    # "roles/bigquery.readSessionUser",
    # "roles/bigquery.jobUser",
    "projects/${var.project_id}/roles/datahub.bigQueryMetadataIngestor",
    # "organizations/${local.organization_id}/roles/datahub.bigQueryMetadataIngestor",
  ])
  project = var.project_id
  role    = each.value
  member  = "serviceAccount:${google_service_account.acryl_ingestor.email}"
}


###############################################################################
#
###############################################################################

# gcloud secrets versions access latest --project="sandbox-iwan-aucamp-e332" --secret="acryl-main-sak"

resource "google_bigquery_table" "coxaitho_000" {
  dataset_id = google_bigquery_dataset.scratchpad.dataset_id
  table_id   = "coxaitho-000"

  deletion_protection = false

  schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
}

resource "google_bigquery_table" "coxaitho_001" {
  dataset_id = google_bigquery_dataset.scratchpad.dataset_id
  table_id   = "coxaitho-001"

  deletion_protection = false

  schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
}

resource "google_bigquery_table" "coxaitho_002" {
  dataset_id = google_bigquery_dataset.scratchpad.dataset_id
  table_id   = "coxaitho-002"

  deletion_protection = false

  schema = file("spec/bigquery/pubsub_bigquery_schemaless.json")
}
